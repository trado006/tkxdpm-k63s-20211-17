package services;

import entity.Bike;

public interface FeeService {
	public int auditFee(long timeRenting, int typeId);
	public int getDeposit(Bike bike);
}
