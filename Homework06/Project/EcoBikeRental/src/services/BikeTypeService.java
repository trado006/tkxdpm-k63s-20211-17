package services;

import java.util.List;
import java.util.Map;

import entity.BikeType;

public interface BikeTypeService {
	public List<BikeType> getAllBikeTypes();
	
	public BikeType getBikeTypeById(int typeId);

}
