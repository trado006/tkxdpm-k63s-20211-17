package services;

import entity.payment.PaymentCard;

public interface CardService<T> {
	public int saveCard(T paymentCard);
}
