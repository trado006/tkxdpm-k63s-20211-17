package services.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import entity.Bike;
import services.BikeService;
import utils.DBConnection;
import utils.Utils;

public class BikeServiceImpl implements BikeService {

	@Override
	public List<Bike> getAllBikes() {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes;");
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	public List<Bike> resultSetToListBike(ResultSet rs) throws SQLException {
		List<Bike> lstBike = new ArrayList<Bike>();
		while (rs.next()) {
			int id = rs.getInt(1);
			int type = rs.getInt(2);
			int stationId = rs.getInt(3);
			boolean isRenting = rs.getBoolean(4);
			int slot = rs.getInt(5);
			String name = rs.getString(6);
			float weight = rs.getFloat(7);
			String licensePlate = rs.getString(8);
			String manufacturer = rs.getString(9);
			Date manufacturingDate = rs.getDate(10);
			int price = rs.getInt(11);
			int batteryPercentage = rs.getInt(12);
			int loadCycles = rs.getInt(13);
			Bike bike = new Bike(id, type, stationId, isRenting, slot, name, weight, manufacturer, licensePlate, price,
					manufacturingDate, loadCycles, batteryPercentage);
			lstBike.add(bike);

		}
		return lstBike;
	}

	@Override
	public List<Bike> getAllRentedBikeByUser(int userId) {
		List<Bike> lstBike = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = "select b.* FROM eco_bike_rental.bikes b, \r\n"
					+ "(select bikeId,max(createAt) as createAt from transactions t " + "where userId=" + userId
					+ " group by bikeId " + ") as c " + "where b.id=c.bikeId and b.isRenting=TRUE";
			ResultSet rs = st.executeQuery(sql);
			lstBike = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return lstBike;
	}

	@Override
	public List<Integer> getAllFillSlotsInStations(int stationId) {
		List<Integer> lstSlot = new ArrayList<>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = "select slot from bikes where isRenting=FALSE and stationId=" + stationId;
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				int slot = rs.getInt(1);
				lstSlot.add(slot);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return lstSlot;
	}

	@Override
	public List<Bike> getBikesByPlate(String plate) {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes WHERE licensePlate LIKE '%" + plate + "%'");
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	@Override
	public List<Bike> getBikesByName(String name) {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes WHERE name LIKE '%" + name + "%'");
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	@Override
	public List<Bike> getBikesByType(String type) {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes INNER JOIN bike_type ON bikes.type = bike_type.id "
					+ "WHERE bike_type.content = '" + type + "'");
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	@Override
	public List<Bike> getBikesByStation(String station) {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes INNER JOIN stations ON bikes.stationId = stations.id "
					+ "WHERE stations.name = '" + station + "'  AND isRenting = 0");
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	@Override
	public List<Bike> getAllRentedBikes() {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes WHERE isRenting = 1");
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	@Override
	public List<Bike> getAllBikeOnAnyStation() {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes WHERE isRenting = 0");
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	@Override
	public boolean addBike(Bike bike) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			int ren;
			if (bike.getRenting())
				ren = 1;
			ren = 0;
			String sql = ("INSERT INTO `eco_bike_rental`.`bikes` (`id`, `type`, `stationId`, `isRenting`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`, `batteryPercentage`, `loadCycles`) "
					+ "VALUES ('" + bike.getId() + "', '" + bike.getType() + "', '" + bike.getStationId() + "', '" + ren
					+ "', '" + bike.getSlot() + "'," + " '" + bike.getName() + "', '" + bike.getWeight() + "', '"
					+ bike.getLicensePlate() + "', '" + bike.getManufacturer() + "', '" + bike.getManufacturingDate()
					+ "'," + " '" + bike.getPrice() + "', '" + bike.getBatteryPercentage() + "', '"
					+ bike.getLoadCycles() + "');");
			st.execute(sql);
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Bike> GetAllBikesByStationId(int stationId) {
		List<Bike> listBikes = new ArrayList<Bike>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bikes INNER JOIN stations ON bikes.stationId = stations.id "
					+ "WHERE stations.id = " + stationId);
			ResultSet rs = st.executeQuery(sql);

			listBikes = resultSetToListBike(rs);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikes;
	}

	@Override
	public boolean updateReturnBike(int bikeId, int stationId, int slot) {

		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("update bikes" 
					+ "	set isRenting=0"
					+ ",stationId=" + stationId
					+ ",slot=" + slot
					+ "	where id=" + bikeId);
			st.execute(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public int getNewId() {
		List<Bike> allBike = getAllBikes();
		boolean id [] = new boolean[100];
		for(Bike bike : allBike) {
			id[bike.getId()] = true;
		}
		for(int i = 1; i <=100; i++) {
			if(!id[i]) return i;
		}
		return 0;
	}

	public List<Bike> searchBikes(HashMap<String, String> searchInfo) {
		List<Bike> listByPlate = null;
		List<Bike> listByName = null;
		List<Bike> listByType = null;
		List<Bike> listByStation = null;
		
		// get lists by each info
		if (searchInfo.get("plate") != "") {
			listByPlate = getBikesByPlate(searchInfo.get("plate"));
			if (listByPlate.size() == 0)
				return listByPlate;
		}
		if (searchInfo.get("name") != "") {
			listByName = getBikesByName(searchInfo.get("name"));
			if (listByName.size() == 0)
				return listByName;
		}
		if (searchInfo.get("type") != "any") {
			listByType = getBikesByType(searchInfo.get("type"));
			if (listByType.size() == 0)
				return listByType;
		}
		if (searchInfo.get("station") == "is_rented") {
			listByStation = getAllRentedBikes();
			if (listByStation.size() == 0)
				return listByStation;
		}
		else if (searchInfo.get("station") == "any") {
			listByStation = getAllBikeOnAnyStation();
			if (listByStation.size() == 0)
				return listByStation;
		}
		else if (searchInfo.get("station") != "both_status"){
			listByStation = getBikesByStation(searchInfo.get("station"));
			if (listByStation.size() == 0)
				return listByStation;
		}
		
		// inner join all lists
		List<List<Bike>> lists = new ArrayList<List<Bike>>();
		lists.add(listByPlate);
		lists.add(listByName);
		lists.add(listByType);
		lists.add(listByStation);
		List<Bike> resultBikes = Utils.joinLists(lists);
		// if all list are null
		if (resultBikes == null) {
			resultBikes = getAllBikes();
		}
		
		return resultBikes;
	}
	
	@Override
	public boolean updateRentBike(int bikeId, int stationId, int slot) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("update bikes" 
					+ "	set isRenting=1"
					+ ",stationId=" + stationId
					+ ",slot=" + slot
					+ "	where id=" + bikeId);
			st.execute(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
