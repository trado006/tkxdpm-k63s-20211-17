package services.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import entity.payment.CreditCard;
import services.CardService;
import utils.DBConnection;

public class CreditCardServiceImpl implements CardService<CreditCard> {

	@Override
	public int saveCard(CreditCard paymentCard) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = "select id from cards" + " where cardcode='" + paymentCard.getCardCode() + "'";
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) {
				return rs.getInt(1);
			} else {
				sql = "INSERT INTO eco_bike_rental.cards" + " (owner, cardcode)" + " VALUES('param1', 'param2')";
				sql = sql.replaceFirst("param1", paymentCard.getOwner());
				sql = sql.replaceFirst("param2", paymentCard.getCardCode());
				st.execute(sql);
				sql = "select id from cards" + " where cardcode='" + paymentCard.getCardCode() + "'";
				rs = st.executeQuery(sql);
				rs.next();
				return rs.getInt(1);

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

}
