package services.impl;

import entity.Bike;
import entity.Session;
import entity.Transaction;
import entity.payment.CreditCard;
import services.BikeService;
import services.CardService;
import services.TransactionService;
import services.UpdateService;

public class UpdateReturnBikeServiceImpl implements UpdateService {

	@Override
	public boolean update() {
		BikeService bikeService = new  BikeServiceImpl();
		TransactionService transactionService = new TransactionServiceImpl();
		CardService<CreditCard> cardService  = new CreditCardServiceImpl();
		//lấy bike từ session
		int cardId = cardService.saveCard((CreditCard)Session.getInvoiceReturnBike().getPaymentCard());
		Bike bike = Session.getInvoiceReturnBike().getBike();
		bikeService.updateReturnBike(bike.getId(), bike.getStationId(),bike.getSlot());
		Transaction transaction = Session.getInvoiceReturnBike().getTransaction();
		transaction.setCardId(cardId);
		transaction.setDescription("update trả xe");
		transactionService.addTransaction(transaction);
		return true;
	}

}
