package services.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.User;
import services.UserService;
import utils.DBConnection;

public class UserServiceImpl implements UserService {
	@Override
	public List<User> getAllUser() {
		List<User> listUser = new ArrayList<User>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM users;");
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next()) { 
				int id = rs.getInt(1); 
				String username = rs.getString(2);
				String password = rs.getString(3);
			 	boolean isAdmin = rs.getBoolean(4);
			 	User user = new User(id, username, password, isAdmin);
			 	listUser.add(user);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listUser;
	}
	
	// return User if username-password exist; otherwise return null
	@Override
	public User checkExistUser(String username, String password) {
		List<User> listUser = getAllUser();
		for (User user : listUser) {
			if (user.getUsername().equals(username)) 
				return user.getPassword().equals(password) ? user : null;
		}
		return null;
	}
}
