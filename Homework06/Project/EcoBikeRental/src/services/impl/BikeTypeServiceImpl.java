package services.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import entity.BikeType;
import services.BikeTypeService;
import utils.DBConnection;

public class BikeTypeServiceImpl implements BikeTypeService{

	@Override
	public List<BikeType> getAllBikeTypes() {
		List<BikeType> listBikeTypes = new ArrayList<BikeType>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bike_type;");
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next()) { 
				int id = rs.getInt(1); 
			 	String content = rs.getString(2);
				int deposit = rs.getInt(3);
				BikeType bikeType = new BikeType(id, content, deposit);
				listBikeTypes.add(bikeType);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listBikeTypes;
	}

	@Override
	public BikeType getBikeTypeById(int typeId) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM bike_type where id="+ typeId);
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next()) { 
				int id = rs.getInt(1); 
			 	String content = rs.getString(2);
				int deposit = rs.getInt(3);
				BikeType bikeType = new BikeType(id, content, deposit);
				return bikeType;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	

}
