package services.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import entity.BikeType;
import entity.Transaction;
import services.TransactionService;
import utils.DBConnection;

public class TransactionServiceImpl implements TransactionService {

	@Override
	public Transaction getLastTranscationbByBikeAndUser(int bikeId, int userId) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("select * from transactions t "
					+ " where userId=" + userId 
					+ " and bikeId=" + bikeId
					+ " ORDER by createAt DESC "
					+ " limit 1");
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next()) { 
				int id = rs.getInt(1);
				int uId= rs.getInt(2);
				int bId= rs.getInt(3);
				int cardId= rs.getInt(4);

				int stationFrom= rs.getInt(5);
				int stationTo= rs.getInt(6);
				int fee= rs.getInt(7);

				String log= rs.getString(8);
				
				Timestamp timestamp = rs.getTimestamp(9);
				Date createAt = new Date(timestamp.getTime());
				String description= rs.getString(10);
				Transaction transaction = new Transaction(id, uId, bId, cardId, stationFrom, stationTo, fee, log, createAt, description);
				return transaction;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	public boolean addTransaction(Transaction transaction) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = "INSERT INTO eco_bike_rental.transactions"
					+ " (userId, bikeId, cardId, stationFrom, stationTo, fee, log, createAt, description)"
					+ " VALUES(param1, param2 , param3, param4, param5, param6, 'param7', CURRENT_TIMESTAMP, 'param8');";
			sql  = sql.replaceFirst("param1",  Integer.toString(transaction.getUserId()));
			sql = sql.replaceFirst("param2", Integer.toString(transaction.getBikeId()));
			sql = sql.replaceFirst("param3", Integer.toString(transaction.getCardId()));
			sql = sql.replaceFirst("param4", Integer.toString(transaction.getStationFrom()));
			sql = sql.replaceFirst("param5", Integer.toString(transaction.getStationTo()));
			sql = sql.replaceFirst("param6", Integer.toString(transaction.getFee()));
			sql = sql.replaceFirst("param7", transaction.getLog());
			sql = sql.replaceFirst("param8", transaction.getDescription());
			st.execute(sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
