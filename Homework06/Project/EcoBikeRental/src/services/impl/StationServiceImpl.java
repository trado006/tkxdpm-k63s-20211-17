package services.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import entity.Station;
import services.BikeService;
import services.StationService;
import utils.DBConnection;

public class StationServiceImpl implements StationService {

	BikeService bikeService = new BikeServiceImpl();

	@Override
	public List<Station> getAllStation() {
		List<Station> listStation = new ArrayList<Station>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM stations;");
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				int maxSlot = rs.getInt(4);
				Station station = new Station(id, name, address, maxSlot);
				listStation.add(station);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listStation;
	}

	@Override
	public List<Station> getStationByName(String stationName) {
		List<Station> listStation = new ArrayList<Station>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM stations WHERE name LIKE '%" + stationName + "%'");
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				int maxSlot = rs.getInt(4);
				Station station = new Station(id, name, address, maxSlot);
				listStation.add(station);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listStation;
	}

	@Override
	public Station getStationById(int stationId) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM eco_bike_rental.stations WHERE id = '" + stationId + "'");
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) { 
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				int maxSlot = rs.getInt(4);
				Station station = new Station(id, name, address, maxSlot);
				return station;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int getNumberOfAvailBike(String stationName) {
		return bikeService.getBikesByStation(stationName).size();
	}

	@Override
	public int getNumberOfAllBike(int stationId) {
		return bikeService.GetAllBikesByStationId(stationId).size();
	}

	@Override
	public List<Station> getStationByAddress(String stationAddress) {
		// TODO Auto-generated method stub
		List<Station> listStation = new ArrayList<Station>();
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM stations WHERE address LIKE '%" + stationAddress + "%'");
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				int maxSlot = rs.getInt(4);
				Station station = new Station(id, name, address, maxSlot);
				listStation.add(station);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listStation;
	}

	@Override
	public boolean checkNameStationAvailable(String stationName) {
		// TODO Auto-generated method stub
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM stations WHERE name = '" + stationName + "'");
			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	@Override
	public boolean addStation(Station station) {
		// TODO Auto-generated method stub
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			
			String sql = ("INSERT INTO `eco_bike_rental`.`stations` (`name`, `address`, `maxSlot`) "
					+ "VALUES ('"+ station.getName() + "', '"+ station.getAddress()+"', '"+ station.getMaxSlot() +"');");
			st.execute(sql);
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
}
