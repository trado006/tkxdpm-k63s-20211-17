package services;

import java.util.List;

import entity.User;

public interface UserService {
	
	public List<User> getAllUser();
	
	public User checkExistUser(String username, String password);
		
}
