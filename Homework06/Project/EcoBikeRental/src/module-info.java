module AIMS {
	requires java.sql;
	requires java.logging;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires org.junit.jupiter.api;
	requires javafx.base;
	requires com.jfoenix;
	requires java.net.http;
	requires org.junit.jupiter.params;
	
	opens application to javafx.graphics, javafx.fxml;
	opens controllers to javafx.graphics, javafx.fxml;
	opens entity to javafx.base;
//	opens views to javafx.graphics, javafx.fxml;
	
	opens views.screen.popup to javafx.graphics, javafx.fxml;
	opens views.screen.login to javafx.graphics, javafx.fxml;
	opens views.screen.returnbikes to javafx.graphics, javafx.fxml;
	opens views.screen.userhomepage to javafx.graphics, javafx.fxml;
	opens views.screen.managebike to javafx.graphics, javafx.fxml;
	opens views.screen.managestation to javafx.graphics, javafx.fxml;
	opens views.screen.adminhomepage to javafx.graphics, javafx.fxml;
	opens views.screen.stationdetail to javafx.graphics, javafx.fxml;
	opens views.screen.bikedetail to javafx.graphics, javafx.fxml;
	opens views.screen.payment to javafx.graphics, javafx.fxml;
	
}
