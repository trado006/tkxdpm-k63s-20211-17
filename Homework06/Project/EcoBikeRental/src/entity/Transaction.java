package entity;

import java.sql.Date;

public class Transaction {
	private int id;
	private int userId;
	private int bikeId;
	private int cardId;
	
	private int stationFrom;
	private int stationTo;
	private int fee;
	
	private String log;
	private Date createAt;
	private String description;
	
	
	public Transaction() {
		
	}
	public Transaction(int id, int userId, int bikeId, int cardId, int stationFrom, int stationTo, int fee, String log,
			Date createAt, String description) {
		super();
		this.id = id;
		this.userId = userId;
		this.bikeId = bikeId;
		this.cardId =cardId;
		this.stationFrom = stationFrom;
		this.stationTo = stationTo;
		this.fee = fee;
		this.log = log;
		this.createAt = createAt;
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getBikeId() {
		return bikeId;
	}
	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}
	public int getStationFrom() {
		return stationFrom;
	}
	public void setStationFrom(int stationFrom) {
		this.stationFrom = stationFrom;
	}
	public int getStationTo() {
		return stationTo;
	}
	public void setStationTo(int stationTo) {
		this.stationTo = stationTo;
	}
	public int getFee() {
		return fee;
	}
	public void setFee(int fee) {
		this.fee = fee;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getCardId() {
		return cardId;
	}
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}
	

}
