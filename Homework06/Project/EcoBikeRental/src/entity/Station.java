package entity;

public class Station {
	private int id;
	private String name;
	private String address;
	private int maxSlot;
	
	public Station() {
		super();
	}
	
	public Station(int id, String name, String address, int maxSlot) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.maxSlot = maxSlot;
	}

	public Station(String name, String address, int maxSlot) {
		super();
		this.name = name;
		this.address = address;
		this.maxSlot = maxSlot;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getMaxSlot() {
		return maxSlot;
	}
	public void setMaxSlot(int maxSlot) {
		this.maxSlot = maxSlot;
	}
	public String toString() {
		return "" + id + " / " + name + " / " + address + " / " + maxSlot;
	}
}
