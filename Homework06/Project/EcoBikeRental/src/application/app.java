package application;

import controllers.AddBikeController;
import controllers.BaseController;
import controllers.LoginController;
import controllers.ManageBikeController;

import java.io.IOException;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.adminhomepage.AdminHomepageHandler;
import views.screen.login.LoginScreenHandler;
import views.screen.managebike.AddBikeHandler;
import views.screen.managebike.ManageBikeHandler;
import views.screen.userhomepage.UserHomepageHandler;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;

public class app extends Application {
	@Override
	public void start(Stage primaryStage) {

//		primaryStage.initStyle(StageStyle.UNDECORATED);
		try {
			// initialize the scene
			StackPane splash = (StackPane) FXMLLoader.load(getClass().getResource(Configs.SPLASH_PATH));
			Scene splashScene = new Scene(splash);
			primaryStage.setScene(splashScene);
			primaryStage.show();

			// Load splash screen with fade in effect
			FadeTransition fadeIn = new FadeTransition(Duration.seconds(1.5), splash);
			fadeIn.setFromValue(0);
			fadeIn.setToValue(1);
			fadeIn.setCycleCount(1);

			// Finish splash with fade out effect
			FadeTransition fadeOut = new FadeTransition(Duration.seconds(1.5), splash);
			fadeOut.setFromValue(1);
			fadeOut.setToValue(0);
			fadeOut.setCycleCount(1);

			// After fade in, start fade out
			fadeIn.play();
			fadeIn.setOnFinished((e) -> {
				fadeOut.play();
			});

			// After fade out, load login screen
			fadeOut.setOnFinished((e) -> {
				try {
					// chuyển thẳng tới màn admin homepage
//					AdminHomepageHandler adminHomepageHandler = new AdminHomepageHandler(primaryStage, Configs.ADMIN_HOMEPAGE_PATH);
//					adminHomepageHandler.show();
					
					// chuyển thẳng tới màn bike manager
//					ManageBikeHandler managerBikeHandler = new ManageBikeHandler(primaryStage, Configs.MANAGE_BIKE_PATH);
//					BaseController baseController = new ManageBikeController();
//					managerBikeHandler.setBController(baseController);
//					managerBikeHandler.show();
					
					// chuyển thẳng tới màn user homepage
//					UserHomepageHandler userHomepageHandler = new UserHomepageHandler(primaryStage, Configs.USER_HOMEPAGE_PATH);
//					BaseController baseController = new UserHomepageController();
//					userHomepageHandler.setBController(baseController);
//					userHomepageHandler.show();
					
					// chuyển tới màn login
					LoginScreenHandler loginScreenHandler = new LoginScreenHandler(primaryStage, Configs.LOGIN_PATH);
					BaseController baseController = new LoginController();
					loginScreenHandler.setBController(baseController);
					loginScreenHandler.show();
					
					//chuyển tới màn addBike
//					AddBikeHandler addBike = new AddBikeHandler(primaryStage, Configs.ADD_BIKE_PATH);
//					BaseController controller = new AddBikeController();
//					addBike.setBController(controller);
//					addBike.show();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
