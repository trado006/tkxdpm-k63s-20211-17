package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

	public static Connection conn = Connect();
	
	private static Connection Connect() {

		try {
			String url = "jdbc:mysql://" + Configs.DB_HOST + ":" + Configs.DB_PORT + "/" + Configs.DB_NAME;
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, Configs.DB_USERNAME, Configs.DB_PASSWORD);
			return conn;
		} catch (ClassNotFoundException | SQLException ex) {
			Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}
}
