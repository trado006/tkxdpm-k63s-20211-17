package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import controllers.validatelicenseplate.ValidateLicensePlateInterface;
import controllers.validatelicenseplate.imp.ValidateLicensePlateImp;
import entity.Bike;
import services.BikeService;
import services.impl.BikeServiceImpl;
import views.screen.popup.PopupScreen;

public class SearchBikeController extends BaseController {
	
	private boolean isInvalidInfo = false;
	
	public List<Bike> processSearchInfo(HashMap<String, String> searchInfo) {
		try {
			validateSearchInfo(searchInfo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// if info is validate, continue search bike
		if (!isInvalidInfo)
			return searchBike(searchInfo);
		// else return null list
		return null;
	}
	
	public void validateSearchInfo(HashMap<String, String> searchInfo) throws IOException {
		if (!validateStatus(searchInfo.get("station"))) {
    		PopupScreen.error("Trạng thái xe không hợp lệ");
    		this.isInvalidInfo = true;
    		return;
    	}
		ValidateLicensePlateInterface validateLicensePlate = new ValidateLicensePlateImp();
    	if (!validateLicensePlate.ValidateLicensePlate(searchInfo.get("plate"))) {
    		PopupScreen.error("Biển số xe không hợp lệ");
    		this.isInvalidInfo = true;
    		return;
    	}
    	if (!validateInputText(searchInfo.get("name"))) {
    		PopupScreen.error("Tên xe không hợp lệ");
    		this.isInvalidInfo = true;
    		return;
    	}
    	this.isInvalidInfo = false;
	}
	
	private boolean validateStatus(String status) {
		if (status == "invalid_status")
			return false;
		return true;
	}
	
	private boolean validateInputText(String text) {
		// check for unique chars
    	for (int i = 0; i < text.length(); i++) {
    		if (!((text.charAt(i) == 32) ||
    			(text.charAt(i) >= 65 && text.charAt(i) <= 90) ||
    			(text.charAt(i) >= 97 && text.charAt(i) <= 122) ||
    			(text.charAt(i) >= 48 && text.charAt(i) <= 57)))
    			return false;
    	}
    	return true;
	}
	
	private List<Bike> searchBike(HashMap<String, String> searchInfo) {
		BikeService bikeService = new BikeServiceImpl();
		return bikeService.searchBikes(searchInfo);
		
	}
	
	private List<Bike> getAllBikes() {
		BikeService bikeService = new BikeServiceImpl();
		return bikeService.getAllBikes();
	}
}
