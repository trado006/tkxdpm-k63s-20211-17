package controllers;
import java.util.List;

import entity.Bike;
import entity.Session;
import services.BikeService;
import services.impl.BikeServiceImpl;
public class ListRentedController extends BaseController  { //implements Initializable

	public ListRentedController() {
		
	}
	
	public List<Bike> getAllRentedBikeByUserLogin(){
		BikeService bikeService = new BikeServiceImpl();
		int userid = Session.getUserLogin().getId();
		List<Bike> lstBike = bikeService.getAllRentedBikeByUser(userid);
		return lstBike;
	}
	

}
