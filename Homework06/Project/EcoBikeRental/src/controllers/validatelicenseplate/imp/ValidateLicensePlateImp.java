package controllers.validatelicenseplate.imp;

import controllers.validatelicenseplate.ValidateLicensePlateInterface;

public class ValidateLicensePlateImp implements ValidateLicensePlateInterface{

	@Override
	public boolean ValidateLicensePlate(String LicensePlate) {
		char arr[] = LicensePlate.toCharArray();
		for(int i = 0; i< arr.length; i++) {
    		if(arr[i] > 90 || (arr[i] < 65 && arr[i] > 57) || (arr[i] < 48 && arr[i] > 45) || (arr[i] < 45 && arr[i] > 32)) return false;
    	}
		return true;
	}

}
