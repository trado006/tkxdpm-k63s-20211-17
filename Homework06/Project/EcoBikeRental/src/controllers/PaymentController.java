package controllers;

import entity.Session;
import entity.User;
import entity.payment.CreditCard;
import entity.payment.InvoiceReturnBike;
import entity.payment.PaymentCard;
import services.UpdateService;
import services.UserService;
import services.impl.UserServiceImpl;

public class PaymentController extends BaseController {
	private UpdateService updateService;

	public UpdateService getUpdateService() {
		return updateService;
	}

	public void setUpdateService(UpdateService updateService) {
		this.updateService = updateService;
	}

	public void updateTransaction() {
		Session.getInvoiceReturnBike().getTransaction().setUserId(Session.getUserLogin().getId());
		updateService.update();
	}

	public void setSessionInvoice(CreditCard creditCard, String logs) {
		InvoiceReturnBike invoiceReturnBike = Session.getInvoiceReturnBike();
		invoiceReturnBike.setPaymentCard(creditCard);
		invoiceReturnBike.getTransaction().setLog(logs);
	}
}
