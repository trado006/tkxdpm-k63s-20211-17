package controllers;

import java.util.List;

import entity.Bike;
import entity.BikeType;
import entity.Station;
import services.BikeService;
import services.BikeTypeService;
import services.StationService;
import services.impl.BikeServiceImpl;
import services.impl.BikeTypeServiceImpl;
import services.impl.StationServiceImpl;

public class ManageBikeController extends BaseController {
	
	public List<Bike> getAllBikes() {
		BikeService bikeService = new BikeServiceImpl();
		return bikeService.getAllBikes();
	}
	
	public List<BikeType> getAllBikeTypes() {
		BikeTypeService bikeTypeService = new BikeTypeServiceImpl();
		return bikeTypeService.getAllBikeTypes();
	}
	
	public BikeType getBikeTypeById(int id) {
		BikeTypeService bikeTypeService = new BikeTypeServiceImpl();
		return bikeTypeService.getBikeTypeById(id);
	}
	
	public Station getStationById(int id) {
		StationService stationService = new StationServiceImpl();
		return stationService.getStationById(id);
	}
	
}
