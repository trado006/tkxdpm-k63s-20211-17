package controllers;

import java.util.List;

import entity.BikeType;
import entity.Session;
import entity.Station;
import entity.User;
import services.BikeTypeService;
import services.StationService;
import services.impl.BikeTypeServiceImpl;
import services.impl.StationServiceImpl;

public class BaseController {
	public User getUserLogin() {
		return Session.getUserLogin();
	}
	
	public List<BikeType> getAllBikeTypes() {
		BikeTypeService bikeTypeService = new BikeTypeServiceImpl();
		return bikeTypeService.getAllBikeTypes();
	}
	
	public List<Station> getAllStations() {
		StationService stationService = new StationServiceImpl();
		return stationService.getAllStation();
	}
}
