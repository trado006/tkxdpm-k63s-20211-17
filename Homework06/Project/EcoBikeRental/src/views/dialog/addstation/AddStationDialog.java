package views.dialog.addstation;

import java.util.Optional;

import controllers.AddStationController;
import entity.Station;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import views.alert.CustomAlert;

public class AddStationDialog {
	private Dialog<Station> dialog;
	
	public AddStationDialog() {
		dialog = new Dialog<>();
        dialog.setTitle("Add Station Dialog");
        dialog.setHeaderText("Add Station");
        
        ButtonType addStationButtonType = new ButtonType("Add Station", ButtonBar.ButtonData.OK_DONE);
        
        dialog.getDialogPane().getButtonTypes().addAll(addStationButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField stationName = new TextField();
        TextField stationAddress = new TextField();
        stationAddress.setPrefWidth(200);
        
        Spinner<Integer> spinner = new Spinner<Integer>();
        int initialValue = 10;
        
        SpinnerValueFactory<Integer> valueFactory = //
                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100, initialValue);
        spinner.setValueFactory(valueFactory);

        grid.add(new Label("Station name:"), 0, 0);
        grid.add(stationName, 1, 0);
        grid.add(new Label("Station address:"), 0, 1);
        grid.add(stationAddress, 1, 1);
        grid.add(new Label("Station packing:"), 0, 2);
        grid.add(spinner, 1, 2);
        
        boolean check[] = {false, false};
        
        Node addStationButton = dialog.getDialogPane().lookupButton(addStationButtonType);
        addStationButton.setDisable(true);
        
        stationName.textProperty().addListener((observable, oldValue, newValue) -> {
        	//System.out.printf(newValue);
        	String name = newValue.trim();
        	check[0] = !name.isEmpty();
        	if(!name.isEmpty()) {
        		AddStationController addStationController = new AddStationController();
        		boolean t = !addStationController.checkStationNameAvailable(name);
        		check[0] = t;
        		if(!t) CustomAlert.error("Station name available\nPlease change station name");
        	}
        	if(check[0]&&check[1]) {
        		addStationButton.setDisable(false);
        	}else {
        		addStationButton.setDisable(true);
        	}
        });
        stationAddress.textProperty().addListener((observable, oldValue, newValue) -> {
        	String name = newValue.trim();
        	check[1] = !name.isEmpty();
        	if(check[0]&&check[1]) {
        		addStationButton.setDisable(false);
        	}else {
        		addStationButton.setDisable(true);
        	}
        });
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == addStationButtonType){
            	String name = stationName.getText().trim();
            	String address = stationAddress.getText().trim();
            	int maxSlot = spinner.getValue();
                return new Station(name, address, maxSlot);
            }
            return null;
        } );
	}
	public Station show() {
		try {
			Optional<Station> result = dialog.showAndWait();
			return result.get();
		}catch(Exception e) {
			return null;
		}
	}
}
