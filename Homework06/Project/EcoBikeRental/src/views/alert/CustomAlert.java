package views.alert;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

public class CustomAlert {
	public static void notice(String contentText) {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Notice");
        alert.setHeaderText("Notice Infomation");
        if(contentText!=null&&contentText!="")
        	alert.setContentText(contentText);

        ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        
        alert.getButtonTypes().setAll(buttonTypeOk);

        alert.show();
	}
	public static void error(String contentText) {
		Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Notice Error");
        if(contentText!=null&&contentText!="")
        	alert.setContentText(contentText);

        ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        
        alert.getButtonTypes().setAll(buttonTypeOk);

        alert.show();
	}
	public static void warning(String contentText) {
		Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText("Notice warnning");
        if(contentText!=null&&contentText!="")
        	alert.setContentText(contentText);

        ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        
        alert.getButtonTypes().setAll(buttonTypeOk);

        alert.show();
	}
	public static boolean confirm(String contentText) {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Notice confirmation");
        if(contentText!=null&&contentText!="")
        	alert.setContentText(contentText);
        
        ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                
        alert.getButtonTypes().setAll(buttonTypeOk, buttonTypeCancel);
        
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get()== buttonTypeOk)
            return true;
        else
        	return false;
	}
}
