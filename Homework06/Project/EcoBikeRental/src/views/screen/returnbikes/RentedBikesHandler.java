package views.screen.returnbikes;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import controllers.BaseController;
import controllers.DetailStatusBikeController;
import controllers.ListRentedController;
import entity.Bike;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.userhomepage.UserHomepageHandler;

public class RentedBikesHandler extends BaseHandler implements Initializable {

	@FXML
	public VBox vBox;


	public RentedBikesHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}



	public void initDataDisplay() {
		vBox.getChildren().clear();
		ListRentedController listRentedController = (ListRentedController) this.getBController();

		List<Bike> lstRentedBikes = listRentedController.getAllRentedBikeByUserLogin();

		for (Bike bike : lstRentedBikes) {
			BikeHandler bikeHandler;
			try {
				bikeHandler = new BikeHandler(Configs.BIKE_PATH, bike);
				bikeHandler.initDataDisplay();
				AnchorPane anchorPane = (AnchorPane)bikeHandler.getContent();
				anchorPane.setOnMouseClicked(e -> {
		            // navigate sang màn detail return
					System.out.println("Đã ấn vào bike " + bike.getName());
					
					BaseHandler detailStatusHandler;
					try {
						detailStatusHandler = new DetailStatusBikeHandler(this.stage, Configs.DETAIL_STATUS_BIKE_PATH, bike);
						BaseController baseController = new DetailStatusBikeController();
						detailStatusHandler.setBController(baseController);
						detailStatusHandler.setPrev(this);
						detailStatusHandler.show();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		        });
				vBox.getChildren().add(anchorPane);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
		}
//		for (int i = 0; i < 8; i++) {
//			try {
//				Bike bike = new Bike();
//				bike.setName("xe " + i);
//				bike.setId(i);
//				bike.setManufacturer("nhà sx " + i);
//				BikeHandler bikeHandler = new BikeHandler(Configs.BIKE_PATH, bike);
//				bikeHandler.initDataDisplay();
//				AnchorPane anchorPane = (AnchorPane)bikeHandler.getContent();
//				anchorPane.setOnMouseClicked(e -> {
//		            // navigate sang màn detail return
//					System.out.println("Đã ấn vào bike " + bike.getName());
//					
//					BaseHandler detailStatusHandler;
//					try {
//						detailStatusHandler = new DetailStatusBikeHandler(this.stage, Configs.DETAIL_STATUS_BIKE_PATH, bike);
//						BaseController baseController = new DetailStatusBikeController();
//						detailStatusHandler.setBController(baseController);
//						detailStatusHandler.setPrev(this);
//						detailStatusHandler.show();
//					} catch (IOException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//		        });
//				vBox.getChildren().add(anchorPane);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}

	}

	
	@FXML
	public void goBack(Event event) {
		try {
			this.getPrev().show();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		vBox.getChildren().clear();
	}
}
