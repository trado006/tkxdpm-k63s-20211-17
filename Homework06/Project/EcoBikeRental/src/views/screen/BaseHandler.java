package views.screen;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import controllers.BaseController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class BaseHandler {
	protected FXMLLoader loader;
	protected Object content;
	protected final Stage stage;
	private BaseHandler prev;
	private Scene scene;
	private BaseController bController;
	protected Hashtable<String, String> messages;

	
	public BaseHandler(String screenPath) throws IOException {
		this.loader = new FXMLLoader(getClass().getResource(screenPath));
		// Set this class as the controller
		this.loader.setController(this);
		this.content = loader.load();
		this.stage = new Stage();
	}

	public BaseHandler(Stage stage, String screenPath) throws IOException {
		this.loader = new FXMLLoader(getClass().getResource(screenPath));
		// Set this class as the controller
		this.loader.setController(this);
		this.content = loader.load();
		this.stage = stage;
	}
	
	public void show() throws IOException {
		if (this.scene == null) {
			this.scene = new Scene((AnchorPane)this.content);
		}
		this.stage.setScene(this.scene);
		initDataDisplay();
		this.stage.show();
	}
	
	public void showAndWait() throws IOException {
		if (this.scene == null) {
			this.scene = new Scene((AnchorPane)this.content);
		}
		this.stage.setScene(this.scene);
		this.stage.initModality(Modality.APPLICATION_MODAL);
		initDataDisplay();
		this.stage.showAndWait();
	}
	
	public void setScreenTitle(String string) {
		this.stage.setTitle(string);
	}

	public void setBController(BaseController bController){
		this.bController = bController;
	}

	public BaseController getBController(){
		return this.bController;
	}

	public void forward(Hashtable messages) {
		this.messages = messages;
	}
	
	public Object getContent() {
		return this.content;
	}

	public FXMLLoader getLoader() {
		return this.loader;
	}

	public void setImage(ImageView imv, String path){
		File file = new File(path);
		Image img = new Image(file.toURI().toString());
		imv.setImage(img);
	}

	public void initDataDisplay() {
		// TODO Auto-generated method stub
		
	}

	public BaseHandler getPrev() {
		return prev;
	}

	public void setPrev(BaseHandler prev) {
		this.prev = prev;
	}
	
	
}
