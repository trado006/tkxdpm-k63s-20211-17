package views.screen.payment;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import controllers.BaseController;
import entity.Bike;
import entity.BikeType;
import entity.Station;
import entity.payment.CreditCard;
import entity.payment.PaymentTransaction;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.BikeService;
import services.BikeTypeService;
import services.StationService;
import services.UpdateService;
import services.impl.BikeServiceImpl;
import services.impl.BikeTypeServiceImpl;
import services.impl.StationServiceImpl;
import services.impl.UpdateReturnBikeServiceImpl;
import subsystem.InterbankSubsystem;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.userhomepage.UserHomepageHandler;

public class ResultHandler extends BaseHandler implements Initializable {

	@FXML
	private Label message;
	
	@FXML
	private VBox vboxContent;

	private PaymentTransaction paymentTransaction;
	
	
	BikeService bikeService = new BikeServiceImpl();
	StationService stationService = new StationServiceImpl();
	BikeTypeService bikeTypeService = new BikeTypeServiceImpl();
	InterbankSubsystem interbankSubsystem = new InterbankSubsystem();

	public ResultHandler(Stage stage, String screenPath, PaymentTransaction paymentTransaction) throws IOException {
		super(stage, screenPath);
		this.paymentTransaction = paymentTransaction;
	}

	public void initDataDisplay() {
		if (paymentTransaction.getResponseCode().equals("00")) {
			message.setText("GIAO DỊCH THÀNH CÔNG");
			Label transIdLabel = new Label("Mã giao dịch: " + paymentTransaction.getTransactionId());
			Label transContentLabel = new Label("Nội dung giao dịch: " + paymentTransaction.getTransactionContent());
			Label transAtLabel = new Label("Thời điểm giao dịch: " + paymentTransaction.getCreatedAt());
			Label transAmountLabel = new Label("Số tiền giao dịch: -" + paymentTransaction.getAmount());
			vboxContent.getChildren().addAll(transIdLabel, transContentLabel, transAtLabel, transAmountLabel);
		} else {
			message.setText("GIAO DỊCH THẤT BẠI");
		}
		Button okBtn = new Button("OK");
		okBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
            	try {			
	            	Stage primaryStage = (Stage) ((Node) me.getSource()).getScene().getWindow();
	    			UserHomepageHandler userHomepageHandler = new UserHomepageHandler(primaryStage, Configs.USER_HOMEPAGE_PATH);
	    			BaseController baseController = new BaseController();
	    			userHomepageHandler.setBController(baseController);
	    			userHomepageHandler.show();
				} catch (Exception e) {
					// TODO: handle exception
				}
            }
		});
		vboxContent.getChildren().add(okBtn);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}

}
