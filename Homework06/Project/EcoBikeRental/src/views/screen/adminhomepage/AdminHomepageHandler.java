package views.screen.adminhomepage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import controllers.BaseController;
import controllers.ManageBikeController;
import controllers.ManageStationController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.managebike.ManageBikeHandler;
import views.screen.managestation.ManageStationHandler;
import views.screen.managestation.TestStationHandler;

public class AdminHomepageHandler extends BaseHandler implements Initializable{
	
	@FXML
	private ImageView imgLogout;
	
	@FXML
	private ImageView manageBikeBtn;
	
	@FXML
    private ImageView manageStationBtn;

	public AdminHomepageHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);

		// fix relative image path caused by fxml
		File file = new File("src/assets/icons/logout.png");
		Image im = new Image(file.toURI().toString());
		imgLogout.setImage(im);
		file = new File("src/assets/icons/xedap.png");
		im = new Image(file.toURI().toString());
		manageBikeBtn.setImage(im);
		file = new File("src/assets/icons/station.png");
		im = new Image(file.toURI().toString());
		manageStationBtn.setImage(im);
		
		manageStationBtn.setOnMouseClicked(e -> {
			try {
				ManageStationHandler bikeHandler = new ManageStationHandler(this.stage, Configs.MANAGE_STATION_PATH);
				BaseController baseController = new ManageStationController();
				bikeHandler.setBController(baseController);
				bikeHandler.setPrev(this);
				bikeHandler.show();
			}
			catch(IOException e1) {
				e1.printStackTrace();
			}
		});
		
		manageBikeBtn.setOnMouseClicked(e -> {
			try {
				ManageBikeHandler bikeHandler = new ManageBikeHandler(this.stage, Configs.MANAGE_BIKE_PATH);
				BaseController baseController = new ManageBikeController();
				bikeHandler.setBController(baseController);
				bikeHandler.setPrev(this);
				bikeHandler.show();
			}
			catch(IOException e1) {
				e1.printStackTrace();
			}
		});
			
		imgLogout.setOnMouseClicked(e -> {
			try {
				this.getPrev().show();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}

}
