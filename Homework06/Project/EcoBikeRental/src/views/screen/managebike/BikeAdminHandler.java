package views.screen.managebike;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import controllers.BaseController;
import controllers.ManageBikeController;
import entity.Bike;
import entity.BikeType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import services.BikeService;
import services.BikeTypeService;
import services.impl.BikeServiceImpl;
import services.impl.BikeTypeServiceImpl;
import utils.Configs;
import views.screen.BaseHandler;

public class BikeAdminHandler extends BaseHandler implements Initializable {

	@FXML
	protected HBox hboxBike;
	
	@FXML
	private Label labelIndex;
	
	@FXML
	private Label labelPlate;
	
	@FXML
	private Label labelName;
	
	@FXML
	private Label labelType;
	
	@FXML
	private Label labelStatus;
	
	@FXML
	private ImageView imgView;
	
	@FXML
	private ImageView imgEdit;
	
	@FXML
	private ImageView imgDelete;
	
	private Bike bike;
	private ManageBikeHandler manageBikeScreen;
	
	public BikeAdminHandler(String screenPath, ManageBikeHandler manageBikeScreen) throws IOException {
		super(screenPath);
		this.manageBikeScreen = manageBikeScreen;
		hboxBike.setAlignment(Pos.CENTER);
		
		// fix relative image path caused by fxml
		File file = new File("src/assets/icons/view.png");
		Image im = new Image(file.toURI().toString());
		imgView.setImage(im);
		file = new File("src/assets/icons/edit.png");
		im = new Image(file.toURI().toString());
		imgEdit.setImage(im);
		file = new File("src/assets/icons/delete.png");
		im = new Image(file.toURI().toString());
		imgDelete.setImage(im);

		imgView.setOnMouseClicked(e -> {
			try {
				DetailBikeAdminHandler detailBikeAdminHandler = new DetailBikeAdminHandler(Configs.DETAIL_BIKE_ADMIN_PATH, this.bike);
				BaseController baseController = new ManageBikeController();
				detailBikeAdminHandler.setBController(baseController);
				detailBikeAdminHandler.setPrev(this.manageBikeScreen);
				detailBikeAdminHandler.showAndWait();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		imgEdit.setOnMouseClicked(e -> {
			// edit bike
		});
		
		imgDelete.setOnMouseClicked(e -> {
			// delete bike
		});
	}
	
	public ManageBikeController getBController() {
		return (ManageBikeController) super.getBController();
	}
	
	public void setBike(Bike bike, int index) {
		this.bike = bike;
		labelIndex.setText(Integer.toString(index));
		labelPlate.setText(bike.getLicensePlate());
		labelName.setText(bike.getName());
		
		// search for bike type
		List<BikeType> listBikeTypes = getBController().getAllBikeTypes();
		for (BikeType type: listBikeTypes) {
			if (bike.getType() == type.getId())
				labelType.setText(type.getContent());
		}
		
		if (bike.getRenting()) {
			labelStatus.setText("Đang được thuê");
			labelStatus.setTextFill(Paint.valueOf("red"));
			imgEdit.setOnMouseClicked(null);
			imgDelete.setOnMouseClicked(null);
		}
		else {
			labelStatus.setText("Đang ở bãi");
			labelStatus.setTextFill(Paint.valueOf("green"));
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
