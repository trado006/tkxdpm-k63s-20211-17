package views.screen.managebike;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import controllers.ManageBikeController;
import entity.Bike;
import entity.BikeType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import utils.Utils;
import views.screen.BaseHandler;

public class DetailBikeAdminHandler extends BaseHandler implements Initializable {

	@FXML
	private Label plate;
	@FXML
	private Label name;
	@FXML
	private Label type;
	@FXML
	private Label status;
	@FXML
	private Label station;
	@FXML
	private Label slot;
	@FXML
	private Label price;
	@FXML
	private Label weight;
	@FXML
	private Label manufacturer;
	@FXML
	private Label date;
	@FXML
	private Label battery;
	@FXML
	private Label load;
	
	@FXML
	private Button btnCancel;
	
	private Bike bike;
	
	public DetailBikeAdminHandler(String screenPath, Bike bike) throws IOException {
		super(screenPath);
		this.bike = bike;
		
		btnCancel.setOnMouseClicked(e -> {
			this.stage.close();
		});
	}
	
	@Override
	public void initDataDisplay() {
		plate.setText(this.bike.getLicensePlate());
		name.setText(this.bike.getName());
		price.setText(Integer.toString(this.bike.getPrice()) + " VND");
		weight.setText(Float.toString(this.bike.getWeight()) + " kg");
		manufacturer.setText(this.bike.getManufacturer());
		date.setText(Utils.formatDate(this.bike.getManufacturingDate()));
		if (this.bike.getType () == 2) {
			battery.setText(Integer.toString(this.bike.getBatteryPercentage()) + "%");
			load.setText(Integer.toString(this.bike.getLoadCycles()));
		}
		else {
			battery.setText("__");
			load.setText("__");
		}
		
		type.setText(getBController().getBikeTypeById(this.bike.getType()).getContent());
		
		if (this.bike.getRenting()) {
			status.setText("Đang thuê");
			status.setTextFill(Paint.valueOf("red"));
			station.setText("__");
			slot.setText("__");
		}
		else {
			status.setText("Đang ở bãi");
			status.setTextFill(Paint.valueOf("green"));
			type.setText(getBController().getStationById(this.bike.getStationId()).getName());
			slot.setText(Integer.toString(this.bike.getSlot()));
		}
	}
	
	public ManageBikeController getBController() {
		return (ManageBikeController) super.getBController();
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
