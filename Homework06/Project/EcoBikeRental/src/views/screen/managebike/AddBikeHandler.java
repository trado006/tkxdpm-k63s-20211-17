package views.screen.managebike;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import controllers.AddBikeController;
import controllers.BaseController;
import controllers.ManageBikeController;
import entity.Bike;
import entity.BikeType;
import entity.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.popup.PopupScreen;

public class AddBikeHandler extends BaseHandler implements Initializable{
	
	private Bike bike;
	
	@FXML
	private TextField name;
	
	@FXML
	private TextField weight;
	
	@FXML
	private TextField licensePlate;
	
	@FXML
	private ComboBox<String> comboType;
	
	@FXML
	private TextField batteryPercentage;
	
	@FXML
	private TextField manufacturer;
	
	@FXML
	private TextField manufacturingDate;
	
	@FXML
	private TextField price;
	
	@FXML
	private ComboBox<String> comboStation;
	
	@FXML
	private TextField loadCycles;
	
	@FXML
	private Button exit;
	
	@FXML
	private Button addBike;
	
	ManageBikeHandler manageBikeScreen;
	
	public AddBikeHandler(String screenPath, ManageBikeHandler manageBikeScreen) throws IOException {
		super(screenPath);
		this.manageBikeScreen = manageBikeScreen;
		
		bike = new Bike();
		AddBikeController controller = new AddBikeController();
		addBike.setOnMouseClicked(e -> {
			
			try {
				if(checkValidate()) {
					if(checkBikeType()){
						if(checkStation()) {
							if(controller.bikeService.addBike(bike)) {
								PopupScreen.success("Đã thêm thành công");
								this.manageBikeScreen.initDataDisplay();
								this.stage.close();
							}
							else {
								PopupScreen.error("Thêm xe thất bại");
								this.stage.close();
							}
								
						}
					}
				}
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		initDataDisplay();
		exit.setOnMouseClicked(e -> {
			this.manageBikeScreen.initDataDisplay();
			this.stage.close();
		});
		
		
	}
	
	
	
	public void initDataDisplay() {
		// init combobox bike type
		AddBikeController controller = new AddBikeController();
		
		List<BikeType> bikeTypes = controller.getAllBikeTypes();
		ObservableList<String> listType = FXCollections.observableArrayList();
		for (BikeType type: bikeTypes) {
			listType.add(type.getContent());
		}
		comboType.setItems(listType);
		
		// init combo box station
		List<Station> stations = controller.getAllStations();
		ObservableList<String> listStation = FXCollections.observableArrayList();
		for (Station station: stations) {
			listStation.add(station.getName());
		}
		comboStation.setItems(listStation);
	}
	
	public boolean checkValidate() throws IOException {
		AddBikeController controller = new AddBikeController();
		String namefie = name.getText();
		String licensePlatefie = licensePlate.getText();
		String manufacturerfie = manufacturer.getText();
		String date = manufacturingDate.getText();
		if(!controller.validateInput(namefie)) {
			PopupScreen.error("Tên xe không hợp lệ");
			return false;
		}
		
		int weightfie;
		try {
			weightfie = Integer.valueOf(weight.getText());
			if(!controller.validateNumber(weightfie)) { 
				PopupScreen.error("trọng lượng không hợp lệ");
				return false;
			}
			else bike.setWeight(weightfie);
		}
		catch(Exception e) {
			PopupScreen.error("trọng lượng không hợp lệ");
		}
		int price;
		try {
			price = Integer.valueOf(weight.getText());
			if(!controller.validateNumber(price)) {
				PopupScreen.error("giá xe không hợp lệ");
				return false;
			}
			else bike.setPrice(price);
		}
		catch(Exception e) {
			PopupScreen.error("giá xe không hợp lệ");
			return false;
		}
		
		if(!controller.licensePlate.ValidateLicensePlate(licensePlatefie)) {
			PopupScreen.error("biển số không hợp lệ");
			return false;
		}
		
		if(!controller.validateInput(manufacturerfie)) {
			PopupScreen.error("nhà sản xuất không hợp lệ");
			return false;
		}
		
		if(!controller.validateManufacturingDate(date)) {
			PopupScreen.error("ngày sản xuất không hợp lệ (11-12-2020)");
			return false;
		}
		bike.setName(namefie);
		bike.setLicensePlate(licensePlatefie);
		bike.setManufacturer(manufacturerfie);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed;
		try {
			parsed = format.parse(fomatDate(date));
			java.sql.Date sql = new java.sql.Date(parsed.getTime());
			bike.setManufacturingDate(sql);
		} catch (ParseException e) {
			
		}
		return true;
	}
	
	public boolean checkBikeType() throws IOException {
		AddBikeController controller = new AddBikeController();
		String type = comboType.getValue();
		try {
			if(type.equalsIgnoreCase("Xe đạp đơn điện") && loadCycles.isDisable()) {
				loadCycles.setDisable(false);
				batteryPercentage.setDisable(false);
				PopupScreen.error("Điền thêm thông tin pin và số lần sạc");
				return false;
			}
			else if(type.equalsIgnoreCase("Xe đạp đơn")) {
				loadCycles.setDisable(true);
				batteryPercentage.setDisable(true);
				bike.setBatteryPercentage(0);
				bike.setLoadCycles(0);
				bike.setType(1);
				
			}
			else if(type.equalsIgnoreCase("Xe đạp đôi")) {
				loadCycles.setDisable(true);
				batteryPercentage.setDisable(true);
				bike.setBatteryPercentage(0);
				bike.setLoadCycles(0);
				bike.setType(3);
			}
			else {
				int battery;
				int load;
				try {
					battery = Integer.valueOf(batteryPercentage.getText());
					if(!controller.validateNumber(battery)) { 
						PopupScreen.error("lượng pin không hợp lệ");
						return false;
					}
					else bike.setBatteryPercentage(battery);
					load = Integer.valueOf(loadCycles.getText());
					if(!controller.validateNumber(load)) { 
						PopupScreen.error("số lần sạc không hợp lệ");
						return false;
					}
					else bike.setLoadCycles(load);
					bike.setType(2);
				}
				catch(Exception e) {
					PopupScreen.error("pin và số lần sạc không hợp lệ");
					return false;
				}
			}
		}
		catch(Exception e) {
			PopupScreen.error("chưa chọn loại xe");
			return false;
		}
		return true;
	
	}
	
	public boolean checkStation() throws IOException {
		AddBikeController controller = new AddBikeController();
		String station = comboStation.getValue();
		try {
			if(station.equalsIgnoreCase("Bãi xe số 1 Đại Cồ Việt")) {
				if(setStaion(1, controller)) return true;
				return false;
			}
			else if(station.equalsIgnoreCase("Bãi xe Ngoại thương")) {
				if(setStaion(2, controller)) return true;
				return false;
			}
			else if(station.equalsIgnoreCase("Bãi xe Học viện Ngân hàng ")) {
				if(setStaion(3, controller)) return true;
				return false;
			}
			else if(station.equalsIgnoreCase("Bãi xe Ga Hà Nội")) {
				if(setStaion(4, controller)) return true;
				return false;
			}
		}
		catch(Exception e) {
			PopupScreen.error("chưa chọn bãi xe");
			return false;
		}
		return true;
	}
	
	private boolean setStaion(int StationId, AddBikeController controller) throws IOException {
		if(controller.stationInfo.checkTotalStation(StationId)) {
			bike.setSlot(controller.stationInfo.getLoacationStation(StationId));
			bike.setId(controller.bikeService.getNewId());
			bike.setStationId(StationId);
			return true;
		}
		else {
			PopupScreen.error("Bãi xe đã đầy, chọn bãi xe khác");
			return false;
		}
	}
	
	private String fomatDate(String date) {
		try {
			int day = Integer.parseInt(date.substring(0, date.indexOf("-")));
	    	date = date.substring(date.indexOf("-") + 1, date.length());
	    	int month = Integer.parseInt(date.substring(0, date.indexOf("-")));
	    	date = date.substring(date.indexOf("-") + 1, date.length());
	    	int year = Integer.parseInt(date);
	    	String days = String.valueOf(day);
	    	String months = String.valueOf(month);
	    	if(day < 10) days = "0" + days;
	    	if(month < 10) months = "0" + months;
	    	return String.valueOf(year) + "-" + months + "-" + days ;
		}
    	catch(Exception e) {
    		return null;
    	}
	}
	
}






