package views.screen.managebike;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import controllers.SearchBikeController;
import entity.Bike;
import entity.BikeType;
import entity.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import views.screen.BaseHandler;

public class SearchBikeHandler extends BaseHandler implements Initializable {

	@FXML
	private TextField textPlate;
	
	@FXML
	private TextField textName;
	
	@FXML
	private ComboBox<String> comboType;
	
	@FXML
	private ComboBox<String> comboStation;
	
	@FXML
	private CheckBox checkBoxIsRented;
	
	@FXML
	private CheckBox checkBoxInStaion;
	
	@FXML
	private Button btnConfirm;
	
	@FXML
	private Button btnCancel;
	
	ManageBikeHandler manageBikeScreen;
	
	public SearchBikeHandler(String screenPath, ManageBikeHandler manageBikeScreen) throws IOException {
		super(screenPath);
		this.manageBikeScreen = manageBikeScreen;
		comboStation.setDisable(true);
		
		checkBoxInStaion.setOnMouseClicked(e -> {
			if (checkBoxInStaion.isSelected()) {
				comboStation.setDisable(false);
			}
			else {
				comboStation.setDisable(true);
			}
		});
		
		btnConfirm.setOnMouseClicked(e -> {
			// set search info
			HashMap searchInfo = new HashMap<>();
			searchInfo.put("plate", textPlate.getText());
			searchInfo.put("name", textName.getText());
			if (comboType.getValue() == "Mọi loại")
				searchInfo.put("type", "any");
			else
				searchInfo.put("type", comboType.getValue());
			if (checkBoxIsRented.isSelected()) {
				if (checkBoxInStaion.isSelected())
					searchInfo.put("station", "both_status");
				else 
					searchInfo.put("station", "is_rented");
			}
			else {
				if (checkBoxInStaion.isSelected()) {
					if (comboStation.getValue() == "Mọi nơi")
						searchInfo.put("station", "any");
					else
						searchInfo.put("station", comboStation.getValue());
				}
				else 
					searchInfo.put("station", "invalid_status");
			}
			
			List<Bike> result = getBController().processSearchInfo(searchInfo);
			// if is not valid result then stay at search bike screen
			if (result != null) {
				this.manageBikeScreen.setListBike(result);
				this.stage.close();
			}
		});
		
		btnCancel.setOnMouseClicked(e -> {
			this.stage.close();
		});
	}
	
	public SearchBikeController getBController() {
		return (SearchBikeController) super.getBController();
	}
	
	public void initDataDisplay() {
		// init combobox bike type
		List<BikeType> bikeTypes = getBController().getAllBikeTypes();
		ObservableList<String> listType = FXCollections.observableArrayList();
		listType.add("Mọi loại");
		for (BikeType type: bikeTypes) {
			listType.add(type.getContent());
		}
		comboType.setItems(listType);
		comboType.setValue("Mọi loại");
		
		// init combo box station
		List<Station> stations = getBController().getAllStations();
		ObservableList<String> listStation = FXCollections.observableArrayList();
		listStation.add("Mọi nơi");
		for (Station station: stations) {
			listStation.add(station.getName());
		}
		comboStation.setItems(listStation);
		comboStation.setValue("Mọi nơi");
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
