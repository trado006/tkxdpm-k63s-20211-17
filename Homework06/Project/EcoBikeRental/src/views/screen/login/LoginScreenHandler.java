package views.screen.login;

import java.io.IOException;
import java.io.ObjectInputFilter.Config;

import controllers.BaseController;
import controllers.LoginController;
import entity.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.adminhomepage.AdminHomepageHandler;
import views.screen.userhomepage.UserHomepageHandler;

public class LoginScreenHandler extends BaseHandler {

	@FXML
	private TextField usernameField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private Button signinBtn;
	@FXML
	private Label errorLabel;
	
	public LoginScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		// TODO Auto-generated constructor stub
	}

	@FXML
	public void login(ActionEvent event) {
		LoginController loginController = new LoginController();
		String username = usernameField.getText();
		String password = passwordField.getText();
		User currentUser = loginController.checkLogin(username, password);
		if (currentUser == null) {
			errorLabel.setText("Sai thông tin đăng nhập");
		} else if (currentUser.isAdmin()) {
			try {
				Stage primaryStage = (Stage)((Node) event.getSource()).getScene().getWindow();
				AdminHomepageHandler adminHomepageHandler = new AdminHomepageHandler(primaryStage, Configs.ADMIN_HOMEPAGE_PATH);
				adminHomepageHandler.setPrev(this);
				adminHomepageHandler.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				Stage primaryStage = (Stage)((Node) event.getSource()).getScene().getWindow();
				UserHomepageHandler userHomepageHandler = new UserHomepageHandler(primaryStage, Configs.USER_HOMEPAGE_PATH);
				BaseController baseController = new BaseController();
				userHomepageHandler.setBController(baseController);
				userHomepageHandler.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
		
	
}
