package views.screen.bikedetail;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import controllers.BaseController;
import controllers.PaymentController;
import entity.Bike;
import entity.BikeType;
import entity.Session;
import entity.Station;
import entity.Transaction;
import entity.payment.CreditCard;
import entity.payment.InvoiceReturnBike;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.BatteryTimeCalculator;
import services.BikeService;
import services.BikeTypeService;
import services.FeeService;
import services.StationService;
import services.impl.BatteryTimeCalculatorImpl;
import services.impl.BikeServiceImpl;
import services.impl.BikeTypeServiceImpl;
import services.impl.FeeServiceImpl;
import services.impl.StationServiceImpl;
import services.impl.UpdateRentBikeServiceImpl;
import services.impl.UpdateReturnBikeServiceImpl;
import utils.Configs;
import utils.Utils;
import views.screen.BaseHandler;
import views.screen.payment.PaymentHandler;
import views.screen.userhomepage.UserHomepageHandler;

public class BikeDetailHandler extends BaseHandler implements Initializable {
	
	@FXML
	private Text name;
	
	@FXML
	private Text weight;
	
	@FXML
	private Text licensePlate;
	
	@FXML
	private Text manufacturer;
	
	@FXML
	private Text manuDate;
	
	@FXML
	private Text price;
	
	@FXML
	private Text deposit;
	
	@FXML
	private GridPane gridPane;
	
	@FXML
	private Button backBtn;
	
	@FXML
	private Button rentBtn;
	
	private Bike bike;
	
	BikeService bikeService = new BikeServiceImpl();
	StationService stationService = new StationServiceImpl();
	BikeTypeService bikeTypeService = new BikeTypeServiceImpl();
	BatteryTimeCalculator battService = new BatteryTimeCalculatorImpl();
	FeeService feeService = new FeeServiceImpl();

	public BikeDetailHandler(Stage stage, String screenPath, Bike bike) throws IOException {
		super(stage, screenPath);
		this.bike = bike;
	}

	public void initDataDisplay() {
		BikeType bikeType = bikeTypeService.getBikeTypeById(bike.getType());
		String type = bikeType.getContent();
		String nameString = type + " - " + bike.getName();
		String dateFormat = Utils.formatDate(bike.getManufacturingDate());
		String priceFormat = Utils.formatMoney(bike.getPrice());
		String depositFormat = Utils.formatMoney(bikeType.getDeposit());
		
		name.setText(nameString.toUpperCase());
		weight.setText(bike.getWeight() + " kg");
		licensePlate.setText(bike.getLicensePlate());
		manufacturer.setText(bike.getManufacturer());
		manuDate.setText(dateFormat);
		price.setText(priceFormat);
		deposit.setText(depositFormat);
		
		if(bikeType.getId() == 2) {
			showElecticInfo();
		}
	}
	
	@FXML
	public void rentBikeBtn(Event event) {
		int deposit = feeService.getDeposit(bike);
		try {
			Stage primaryStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			PaymentHandler paymentHandler = new PaymentHandler(primaryStage, Configs.PAYMENT_PATH, deposit);
			PaymentController baseController = new PaymentController();
			
			Session session = new Session();
			Transaction transaction = new Transaction();
			transaction.setBikeId(bike.getId());
			transaction.setUserId(session.getUserLogin().getId());
			transaction.setFee(deposit);
			transaction.setStationFrom(bike.getStationId());
			transaction.setStationTo(bike.getStationId());
			CreditCard card = new CreditCard(null, null, null, null);
			InvoiceReturnBike invoice = new InvoiceReturnBike(bike, card, transaction);
			session.setInvoiceReturnBike(invoice);
			
			baseController.setUpdateService(new UpdateRentBikeServiceImpl());
			paymentHandler.setBController(baseController);
			paymentHandler.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void goBack(Event event) {
		try {
			Stage primaryStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			UserHomepageHandler userHomepageHandler = new UserHomepageHandler(primaryStage, Configs.USER_HOMEPAGE_PATH);
			BaseController baseController = new BaseController();
			userHomepageHandler.setBController(baseController);
			userHomepageHandler.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void showElecticInfo () {
		int batteryPercentage = bike.getBatteryPercentage();
		
		Text batteryText = new Text("Dung lượng pin: ");
		Text batteryValue = new Text(batteryPercentage + "");
		Text loadCyclesText = new Text("Số lần sạc: ");
		Text loadCyclesValue = new Text(bike.getLoadCycles() + "");			
		Text timeEstimateText = new Text("Thời gian còn lại: ");
		Text timeEstimateValue = new Text(battService.estimateTimeRemain(batteryPercentage));	
		
		batteryText.getStyleClass().add("electText");
		loadCyclesText.getStyleClass().add("electText");
		timeEstimateText.getStyleClass().add("electText");
		batteryValue.getStyleClass().add("electBoldText");
		loadCyclesValue.getStyleClass().add("electBoldText");
		timeEstimateValue.getStyleClass().add("electBoldText");
		
		gridPane.add(batteryText, 0, 0);
		gridPane.add(batteryValue, 1, 0);
		gridPane.add(loadCyclesText, 0, 1);
		gridPane.add(loadCyclesValue, 1, 1);
		gridPane.add(timeEstimateText, 0, 2);
		gridPane.add(timeEstimateValue, 1, 2);
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
}
