package services.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import entity.Bike;

class BikeServiceImplTest {

	private BikeServiceImpl bikeServiceImpl;
	@BeforeEach
	void setUp() throws Exception {
		bikeServiceImpl = new BikeServiceImpl();
	}

	@Test
	void test() {
		List<Bike> lst = bikeServiceImpl.getAllRentedBikeByUser(0);
		assertEquals(0, lst.size());
	}

}
