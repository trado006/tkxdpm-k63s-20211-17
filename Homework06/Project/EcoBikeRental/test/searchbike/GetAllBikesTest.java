package searchbike;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import entity.Bike;
import services.BikeService;
import services.impl.BikeServiceImpl;

class GetAllBikesTest {
	private BikeService bikeService;
	private List<Integer> expected;
	private List<Integer> result;
	
	@BeforeEach
	void setUp() throws Exception {
		bikeService = new BikeServiceImpl();
		expected = new ArrayList<>();
		result = new ArrayList<>();
	}

	@Test
	void test() {
		// init expected
		for (int i = 1; i <= 12; ++i) {
			expected.add(i);
		}
		//when
		List<Bike> list = bikeService.getAllBikes();
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}

}
