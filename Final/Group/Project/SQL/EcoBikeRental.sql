CREATE DATABASE IF NOT EXISTS eco_bike_rental;
USE eco_bike_rental;

CREATE TABLE IF NOT EXISTS users (
	id INT AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL,
    isAdmin BOOLEAN DEFAULT 0
);

CREATE TABLE IF NOT EXISTS stations (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(50) NOT NULL UNIQUE,
    address VARCHAR(50) NOT NULL,
    maxSlot INT NOT NULL
);

CREATE TABLE IF NOT EXISTS bike_type (
	id INT AUTO_INCREMENT PRIMARY KEY,
	content VARCHAR(50) NOT NULL UNIQUE,
    deposit INT
);

CREATE TABLE IF NOT EXISTS bikes (
	id INT AUTO_INCREMENT PRIMARY KEY,
	type INT NOT NULL,
    stationId INT NOT NULL,
    isRenting BOOLEAN DEFAULT 0,
    slot INT,
    name VARCHAR(50) NOT NULL,
    weight DECIMAL(6,2),
    licensePlate VARCHAR(10) NOT NULL,
    manufacturer VARCHAR(50),
    manufacturingDate DATE,
    price INT NOT NULL,
    batteryPercentage INT DEFAULT NULL,
    loadCycles INT DEFAULT NULL,
	FOREIGN KEY (type) REFERENCES bike_type(id),
    FOREIGN KEY (stationId) REFERENCES stations(id)
);

CREATE TABLE IF NOT EXISTS cards (
	id INT AUTO_INCREMENT PRIMARY KEY,
	cardcode VARCHAR(50) NOT NULL,
    owner VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS transactions (
	id INT AUTO_INCREMENT PRIMARY KEY,
	userId INT,
    bikeId INT,
	cardId INT,
    stationFrom INT,
    stationTo INT,
    fee INT,
    log VARCHAR(50),
    createAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    description VARCHAR(500),
    FOREIGN KEY (userId) REFERENCES users(id),
    FOREIGN KEY (bikeId) REFERENCES bikes(id),
    FOREIGN KEY (cardId) REFERENCES cards(id),
    FOREIGN KEY (stationFrom) REFERENCES stations(id),
    FOREIGN KEY (stationTo) REFERENCES stations(id)
);

INSERT INTO `eco_bike_rental`.`users` (`username`, `password`) VALUES ('user1', '123456');
INSERT INTO `eco_bike_rental`.`users` (`username`, `password`) VALUES ('user2', '123456');
INSERT INTO `eco_bike_rental`.`users` (`username`, `password`) VALUES ('user3', '123456');
INSERT INTO `eco_bike_rental`.`users` (`username`, `password`, `isAdmin`) VALUES ('admin1', '123456', '1');
INSERT INTO `eco_bike_rental`.`users` (`username`, `password`, `isAdmin`) VALUES ('admin2', '123456', '1');

INSERT INTO `eco_bike_rental`.`bike_type` (`content`, `deposit`) VALUES ('Xe đạp đơn', 400000);
INSERT INTO `eco_bike_rental`.`bike_type` (`content`, `deposit`) VALUES ('Xe đạp đơn điện', 700000);
INSERT INTO `eco_bike_rental`.`bike_type` (`content`, `deposit`) VALUES ('Xe đạp đôi', 550000);

INSERT INTO `eco_bike_rental`.`stations` (`name`, `address`, `maxSlot`) VALUES ('Bãi xe số 1 Đại Cồ Việt', '1 Đại Cồ Việt, Bách Khoa, Hai Bà Trưng', 12);
INSERT INTO `eco_bike_rental`.`stations` (`name`, `address`, `maxSlot`) VALUES ('Bãi xe Ngoại thương', '91 Chùa Láng, Láng Thượng, Đống Đa', 11);
INSERT INTO `eco_bike_rental`.`stations` (`name`, `address`, `maxSlot`) VALUES ('Bãi xe Học viện Ngân hàng ', '12 P. Chùa Bộc, Quang Trung, Đống Đa', 9);
INSERT INTO `eco_bike_rental`.`stations` (`name`, `address`, `maxSlot`) VALUES ('Bãi xe Ga Hà Nội', 'Số 120, phố Lê Duẩn, quận Đống Đa', 10);

INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('1', '1', '2', 'Xe đạp thể thao 1', '20.13', 'GH-4716', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '3600000');
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `isRenting`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('1', '1', '1', '5', 'Xe đạp thể thao 2', '21.54', 'GH-5645', 'Công Ty TNHH Huy Ngọc', '2019-12-26', '3550000');
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `isRenting`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`, `batteryPercentage`, `loadCycles`) VALUES ('2', '2', '1', '3', 'Xe đạp điện thể thao 1', '47.29', 'EQ-4362', 'Công Ty TNHH Huy Ngọc', '2020-07-24', '7600000', 78, 2);
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('3', '1', '7', 'Xe đạp đôi thể thao 1', '28.36', 'PH-4876', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '5500000');
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`, `batteryPercentage`, `loadCycles`) VALUES ('2', '2', '8', 'Xe đạp điện thể thao 2', '49.54', 'EQ-1975', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '8700000', 84, 1);
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `isRenting`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('3', '2', '1', '1', 'Xe đạp đôi thể thao 2', '29.31', 'PH-4664', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '5440000');
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('1', '1', '4', 'Xe đạp thể thao 3', '21.23', 'GH-2394', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '3430000');
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`, `batteryPercentage`, `loadCycles`) VALUES ('2', '3', '6', 'Xe đạp điện thể thao 3', '41.33', 'EQ-3765', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '7670000', 67, 3);
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`, `batteryPercentage`, `loadCycles`) VALUES ('2', '1', '8', 'Xe đạp điện thể thao 4', '40.24', 'EQ-4616', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '7940000', 43, 2);
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('3', '4', '1', 'Xe đạp đôi thể thao 3', '38.19', 'PH-3712', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '5460000');
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('3', '1', '1', 'Xe đạp đôi thể thao 4', '37.64', 'PH-4666', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '4940000');
INSERT INTO `eco_bike_rental`.`bikes` (`type`, `stationId`, `isRenting`, `slot`, `name`, `weight`, `licensePlate`, `manufacturer`, `manufacturingDate`, `price`) VALUES ('1', '2', '1', '5', 'Xe đạp thể thao 4', '29.55', 'GH-3126', 'Công Ty TNHH Huy Ngọc', '2020-11-06', '3630000');

INSERT INTO `eco_bike_rental`.`cards` (`cardcode`, `owner`) VALUES ('0123456789', 'aaa');
INSERT INTO `eco_bike_rental`.`cards` (`cardcode`, `owner`) VALUES ('0987654321', 'bbb');
INSERT INTO `eco_bike_rental`.`cards` (`cardcode`, `owner`) VALUES ('0246813579', 'ccc');

INSERT INTO `eco_bike_rental`.`transactions` (`userId`, `bikeId`, `cardId`, `stationFrom`, `stationTo`, `fee`, `description`) VALUES ('1', '1', '1', '1', '2', '100000', 'Thuê xe đạp đơn có mã 001');
INSERT INTO `eco_bike_rental`.`transactions` (`userId`, `bikeId`, `cardId`, `stationFrom`, `stationTo`, `fee`, `description`) VALUES ('2', '2', '2', '2', '4', '850000', 'Thuê xe đạp đơn có mã 002');
INSERT INTO `eco_bike_rental`.`transactions` (`userId`, `bikeId`, `cardId`, `stationFrom`, `stationTo`, `fee`, `description`) VALUES ('3', '3', '3', '3', '1', '60000', 'Thuê xe đạp điện đơn có mã 001');
