package searchbike;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import entity.Bike;
import services.BikeService;
import services.impl.BikeServiceImpl;

class SearchBikeTest {
	private BikeService bikeService;
	private List<Integer> expected;
	private List<Integer> result;
	HashMap searchInfo;
	
	
	@BeforeEach
	void setUp() throws Exception {
		bikeService = new BikeServiceImpl();
		expected = new ArrayList<>();
		result = new ArrayList<>();
		searchInfo = new HashMap<>();
	}

	@Test
	void testCase1() {
		// init expected

		//when
		searchInfo.put("plate", "12345");
		searchInfo.put("name", "");
		searchInfo.put("type", "any");
		searchInfo.put("station", "any");
		List<Bike> list = bikeService.searchBikes(searchInfo);
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}

	@Test
	void testCase2() {
		// init expected

		//when
		searchInfo.put("plate", "GH");
		searchInfo.put("name", "123");
		searchInfo.put("type", "any");
		searchInfo.put("station", "any");
		List<Bike> list = bikeService.searchBikes(searchInfo);
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase3() {
		// init expected

		//when
		searchInfo.put("plate", "");
		searchInfo.put("name", "Xe đạp đôi");
		searchInfo.put("type", "Xe đạp lạ");
		searchInfo.put("station", "any");
		List<Bike> list = bikeService.searchBikes(searchInfo);
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase4() {
		// init expected
		expected.add(2); expected.add(12);
		
		//when
		searchInfo.put("plate", "");
		searchInfo.put("name", "");
		searchInfo.put("type", "Xe đạp đơn");
		searchInfo.put("station", "is_rented");
		List<Bike> list = bikeService.searchBikes(searchInfo);
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase5() {
		// init expected
		for (int i = 1; i <= 12; ++i) {
			expected.add(i);
		}
		
		//when
		searchInfo.put("plate", "");
		searchInfo.put("name", "");
		searchInfo.put("type", "any");
		searchInfo.put("station", "both_status");
		List<Bike> list = bikeService.searchBikes(searchInfo);
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase6() {
		// init expected
		expected.add(1); expected.add(4); 
		expected.add(5); expected.add(7); expected.add(8); 
		expected.add(9); expected.add(10); expected.add(11); 

		//when
		searchInfo.put("plate", "");
		searchInfo.put("name", "");
		searchInfo.put("type", "any");
		searchInfo.put("station", "any");
		List<Bike> list = bikeService.searchBikes(searchInfo);
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase7() {
		// init expected

		//when
		searchInfo.put("plate", "");
		searchInfo.put("name", "");
		searchInfo.put("type", "any");
		searchInfo.put("station", "Bãi không người dùng");
		List<Bike> list = bikeService.searchBikes(searchInfo);
		for (int i = 0; i < list.size(); ++i) {
			result.add(list.get(i).getId());
		}
		//then
		assertEquals(expected, result);
	}

}
