package searchbike;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import entity.Bike;
import utils.Utils;

class JoinListsTest {
	private List<Bike> expected;
	private List<Bike> result;
	
	private Bike bikeA, bikeB, bikeC;
	private List<Bike> listA, listB;
	
	@BeforeEach
	void setUp() throws Exception {
		expected = new ArrayList<>();
		result = new ArrayList<>();
		
		bikeA = new Bike(1, 1, 1, true, 1, "", 0, "", "", 1, null,  0, 0);
		bikeB = new Bike(2, 1, 1, true, 1, "", 0, "", "", 1, null,  0, 0);
		bikeC = new Bike(3, 1, 1, true, 1, "", 0, "", "", 1, null,  0, 0);
		
		listA = null;
		listB = null;
	}

	@Test
	void testCase1() {
		// init expected
		expected.add(bikeA);

		//input
		listA = new ArrayList<>(); listA.add(bikeA); listA.add(bikeB); 
		listB = new ArrayList<>(); listB.add(bikeA); listB.add(bikeC); 
		List<List<Bike>> lists = new ArrayList<List<Bike>>();
		lists.add(listA);
		lists.add(listB);
		result = Utils.joinLists(lists);
		
		//then
		assertEquals(expected, result);
	}

	@Test
	void testCase2() {
		// init expected

		//input
		listA = new ArrayList<>(); listA.add(bikeA); listA.add(bikeB); 
		listB = new ArrayList<>();
		List<List<Bike>> lists = new ArrayList<List<Bike>>();
		lists.add(listA);
		lists.add(listB);
		result = Utils.joinLists(lists);
		
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase3() {
		// init expected
		expected.add(bikeA);
		expected.add(bikeB);
		
		//input
		listA = new ArrayList<>(); listA.add(bikeA); listA.add(bikeB); 

		List<List<Bike>> lists = new ArrayList<List<Bike>>();
		lists.add(listA);
		lists.add(listB);
		result = Utils.joinLists(lists);
		
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase4() {
		// init expected
		
		//input
		listA = new ArrayList<>();

		List<List<Bike>> lists = new ArrayList<List<Bike>>();
		lists.add(listA);
		lists.add(listB);
		result = Utils.joinLists(lists);
		
		//then
		assertEquals(expected, result);
	}
	
	@Test
	void testCase5() {
		// init expected
		expected = null;
		
		//input

		List<List<Bike>> lists = new ArrayList<List<Bike>>();
		lists.add(listA);
		lists.add(listB);
		result = Utils.joinLists(lists);
		
		//then
		assertEquals(expected, result);
	}

}
