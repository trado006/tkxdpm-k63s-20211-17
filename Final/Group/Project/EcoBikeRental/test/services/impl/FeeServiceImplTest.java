package services.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import services.FeeService;

class FeeServiceImplTest {

	private FeeService feeService;

	@BeforeEach
	void setUp() throws Exception {
		feeService = new FeeServiceImpl();
	}

	@ParameterizedTest
	@CsvSource({ "9, 1, 0", 
		"10, 1, 10000",
		"29,1,10000",
		"30,1,10000",
		"31,1,13000"})
	void testBlackBox(int timeMinute, int type, int expected) {
		int fee = feeService.auditFee(timeMinute, type);
		assertEquals(expected, fee);
	}
	
	@ParameterizedTest
	@CsvSource({ 
		"8, 1, 0",
		"8, 2, 0", 
		"20, 1, 10000",
		"20, 2, 15000",
		"40, 1, 13000", 
		"40, 2, 19500" })
	void testWhiteBox(int timeMinute, int type, int expected) {
		int fee = feeService.auditFee(timeMinute, type);
		assertEquals(expected, fee);
	}

}
