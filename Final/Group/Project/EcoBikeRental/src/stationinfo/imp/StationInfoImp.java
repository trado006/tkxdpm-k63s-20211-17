package stationinfo.imp;

import java.util.List;

import entity.Bike;
import entity.Station;
import services.BikeService;
import services.StationService;
import services.impl.BikeServiceImpl;
import services.impl.StationServiceImpl;
import stationinfo.StationInfoInterface;

public class StationInfoImp implements StationInfoInterface{
	StationService service;
	
	public StationInfoImp() {
		service = new StationServiceImpl();
	}
	@Override
	public int getLoacationStation(int stationId) {
		BikeService bikeService = new BikeServiceImpl();
		List<Bike> listbike = bikeService.GetAllBikesByStationId(stationId);
		Station station = service.getStationById(stationId);
		boolean list [] = new boolean[station.getMaxSlot() + 2];
		for(Bike i : listbike) {
			list[i.getSlot()] = true;
		}
		for(int i = 1; i <= station.getMaxSlot(); i++) {
			if(!list[i]) return i;
		}
		return -1;
	}

	@Override
	public boolean checkTotalStation(int stationId) {
		Station station = service.getStationById(stationId);
		int allbike = service.getNumberOfAllBike(stationId);
		if(allbike < station.getMaxSlot()) return true;
		return false;
	}

}
