package utils;

public class Configs {
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final int RENT_HOUR_FEE = 10000;
	
	// api constants
	public static final String PROCESS_TRANSACTION_URL = "https://ecopark-system-api.herokuapp.com/api/card/processTransaction";
	public static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIxMTg2MDlfZ3JvdXAxXzIwMjAiLCJpYXQiOjE1OTkxMTk5NDl9.y81pBkM0pVn31YDPFwMGXXkQRKW5RaPIJ5WW5r9OW-Y";
	
	// static resources
	public static final String POP_UP_PATH = "/views/PopUp.fxml";
	public static final String SPLASH_PATH = "/views/SplashScreen.fxml";
	public static final String LOGIN_PATH = "/views/LoginScreen.fxml";
	public static final String USER_HOMEPAGE_PATH = "/views/UserHomePage.fxml";
	public static final String ADMIN_HOMEPAGE_PATH = "/views/AdminHomepage.fxml";
	public static final String STATION_DETAIL_PATH = "/views/StationDetailScreen.fxml";
	public static final String BIKE_DETAIL_PATH = "/views/BikeDetailScreen.fxml";
	public static final String RESULT_SCREEN_PATH = "/views/ResultScreen.fxml";
	public static final String LIST_RENTED_BIKE_PATH = "/views/ListRentBike.fxml";
	public static final String BIKE_PATH = "/views/Bike.fxml";
	public static final String DETAIL_STATUS_BIKE_PATH = "/views/DetailStatusBike.fxml";
	public static final String PAYMENT_PATH = "/views/PaymentScreen.fxml";
	public static final String MANAGE_BIKE_PATH = "/views/ManageBikeScreen.fxml";
	public static final String MANAGE_STATION_PATH = "/views/ManageStationScreen.fxml";
	public static final String BIKE_ADMIN_PATH = "/views/BikeAdmin.fxml";
	public static final String SEARCH_BIKE_PATH = "/views/SearchBikeScreen.fxml";
	public static final String ADD_BIKE_PATH = "/views/AddBike.fxml";
	public static final String DETAIL_BIKE_ADMIN_PATH = "/views/DetailBikeAdminScreen.fxml";
	
	public static String DB_NAME = "eco_bike_rental";
	public static String DB_USERNAME = "root";
	public static String DB_PASSWORD = "mysql";
	public static String DB_HOST = "localhost";
	public static String DB_PORT = "3306";

	
}
