package controllers;


import entity.Session;
import entity.User;
import services.UserService;
import services.impl.UserServiceImpl;

public class LoginController extends BaseController {

	public User checkLogin(String username, String password) {

		UserService userService = new UserServiceImpl();
		User user = userService.checkExistUser(username, password);
		if (user != null) {
			user.setPassword("");
			Session.setUserLogin(user);
		}
		return user;

	}
}
