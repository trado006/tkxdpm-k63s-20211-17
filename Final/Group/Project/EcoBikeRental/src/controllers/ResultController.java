package controllers;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import entity.Bike;
import entity.Session;
import entity.Station;
import entity.Transaction;
import services.BikeService;
import services.FeeService;
import services.StationService;
import services.TransactionService;
import services.impl.BikeServiceImpl;
import services.impl.FeeServiceImpl;
import services.impl.StationServiceImpl;
import services.impl.TransactionServiceImpl;

public class ResultController extends BaseController {
	public ResultController() {

	}

	public Map<Station, List<Integer>> getStatusStations() {
		Map<Station, List<Integer>> map_rs = new HashMap<>();
		StationService stationService = new StationServiceImpl();
		BikeService bikeService = new BikeServiceImpl();
		List<Station> lstStation = stationService.getAllStation();
		for (Station station : lstStation) {
			List<Integer> lstFillSlotinStation = bikeService.getAllFillSlotsInStations(station.getId());
			List<Integer> lstNotFillSlot = getFreeSlotinStation(station, lstFillSlotinStation);
			map_rs.put(station, lstNotFillSlot);
		}
		return map_rs;
	}

	public List<Integer> getFreeSlotinStation(Station station, List<Integer> lstFillSlotinStation) {
		List<Integer> lst_rs = new ArrayList<>();
		for (int i = 1; i <= station.getMaxSlot(); i++) {
			if(!lstFillSlotinStation.contains(i)) {
				lst_rs.add(i);
			}
		}
		return lst_rs;
	}
	
	public int getDepositFee(int bikeId) {
		TransactionService transactionService = new TransactionServiceImpl();
		Transaction transaction = transactionService.getLastTranscationbByBikeAndUser(bikeId,Session.getUserLogin().getId());
		return transaction.getFee();
	}
	
	public Date getStartTime(int bikeId) {
		TransactionService transactionService = new TransactionServiceImpl();
		Transaction transaction = transactionService.getLastTranscationbByBikeAndUser(bikeId,Session.getUserLogin().getId());
		return transaction.getCreateAt();
	}
	
	public int getFeeTotal(int bikeId, int typeId) {
		TransactionService transactionService = new TransactionServiceImpl();
		Transaction transaction = transactionService.getLastTranscationbByBikeAndUser(bikeId,Session.getUserLogin().getId());
		Date startTime = transaction.getCreateAt();
		Instant instant = LocalDateTime.now().toInstant(ZoneOffset.UTC);
	    java.util.Date now = Date.from(instant);
		long diff = now.getTime() - startTime.getTime();
		long retingTime = TimeUnit.MILLISECONDS.toMinutes(diff); 
		
		FeeService feeService = new FeeServiceImpl();
		int fee = feeService.auditFee(retingTime, typeId);
		return fee;
	}
}
