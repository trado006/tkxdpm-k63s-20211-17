package controllers;

import controllers.validatelicenseplate.ValidateLicensePlateInterface;
import controllers.validatelicenseplate.imp.ValidateLicensePlateImp;
import entity.Bike;
import services.BikeService;
import services.impl.BikeServiceImpl;
import stationinfo.StationInfoInterface;
import stationinfo.imp.StationInfoImp;

public class AddBikeController extends BaseController{
	
	public ValidateLicensePlateInterface licensePlate = new ValidateLicensePlateImp();
	public StationInfoInterface stationInfo = new StationInfoImp();
	public BikeService bikeService = new BikeServiceImpl();
		
    public boolean validateInput(String name) {
    	if(name.length() == 0) return false;
    	char arr[] = name.toCharArray();
    	for(int i = 0; i< arr.length; i++) {
    		if(arr[i] > 122 || (arr[i] < 97 && arr[i] > 90) || (arr[i] < 65 && arr[i] > 57) || (arr[i] < 48 && arr[i] > 32)) return false;
    	}
    	return true;
    }
    
    public boolean validateNumber(int weight) {
    	if(weight <= 0) return false;
    	return true;
    }
    
    public boolean validateManufacturingDate(String date) {
    	if(date == null) return false;
    	try {
	    	int day = Integer.parseInt(date.substring(0, date.indexOf("-")));
	    	date = date.substring(date.indexOf("-") + 1, date.length());
	    	int month = Integer.parseInt(date.substring(0, date.indexOf("-")));
	    	date = date.substring(date.indexOf("-") + 1, date.length());
	    	int year = Integer.parseInt(date);
	    	
	    	if((day <= 31 && day > 0) && (month <= 12 && month > 0) && (year < 2023 && year > 2000)) return true;
	    	return false;
    	}
    	catch(Exception e) {
    		return false;
    	}
    }
    
	boolean AddBike(Bike bike) {
		BikeService service = new BikeServiceImpl();
		if(service.addBike(bike))
			return true;
		return false;
	}
    
}
