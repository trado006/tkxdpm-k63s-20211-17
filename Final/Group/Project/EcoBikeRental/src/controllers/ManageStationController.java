package controllers;

import java.util.List;

import entity.Station;
import services.StationService;
import services.impl.StationServiceImpl;

public class ManageStationController extends BaseController {
	
	public List<Station> getAllStation() {
		StationService stationService = new StationServiceImpl();
		return stationService.getAllStation();
	}
	
	public List<Station> getStationByName(String stationName){
		StationService stationService = new StationServiceImpl();
		return stationService.getStationByName(stationName);
	}
	
	public List<Station> getStationByAddress(String stationAddress){
		StationService stationService = new StationServiceImpl();
		return stationService.getStationByAddress(stationAddress);
	}
}
