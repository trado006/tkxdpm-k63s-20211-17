package controllers;

import entity.Station;
import services.StationService;
import services.impl.StationServiceImpl;

public class AddStationController extends BaseController{
	
	public boolean addStation(Station station) {
		StationService service = new StationServiceImpl();
		if(service.addStation(station)) {
			return true;
		}
		return false;
	}
	
	public boolean checkStationNameAvailable(String stationName) {
		StationService service = new StationServiceImpl();
		return service.checkNameStationAvailable(stationName);
	}
	
    public boolean validateInput(String name) {
    	if(name == null) return false;
    	char arr[] = name.toCharArray();
    	for(int i = 0; i< arr.length; i++) {
    		if(arr[i] > 122 || (arr[i] < 97 && arr[i] > 90) || (arr[i] < 65 && arr[i] > 57) || (arr[i] < 48 && arr[i] > 32)) return false;
    	}
    	return true;
    }
    
    public boolean validateMaxSlot(int maxSlot) {
    	if(maxSlot <= 0) return false;
    	return true;
    }
}
