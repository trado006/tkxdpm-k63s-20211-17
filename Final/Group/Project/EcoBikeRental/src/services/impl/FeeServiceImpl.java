package services.impl;

import entity.Bike;
import entity.BikeType;
import services.BikeTypeService;
import services.FeeService;

public class FeeServiceImpl implements FeeService {

	@Override
	public int auditFee(long timeRenting, int typeId) {
		int fee = 0;
		if (timeRenting < 10) {
			fee = 0;
		}else if (timeRenting < 30) {
			fee = 10000;
		}else {
			fee = 10000;
			fee += 3000 * Math.ceil((float)(timeRenting-30)/15);
		}
		if(typeId != 1) {
			fee = (int) (fee * 1.5);
		}
		return fee;
	}

	@Override
	public int getDeposit(Bike bike) {
		BikeTypeService bikeTypeService = new BikeTypeServiceImpl();
		BikeType bikeType = bikeTypeService.getBikeTypeById(bike.getType());
		return bikeType.getDeposit() / 1000;
	}

}
