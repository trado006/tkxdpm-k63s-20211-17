package services.impl;

import java.sql.Connection;
import java.sql.Statement;

import services.SupportService;
import utils.DBConnection;

public class SupportServiceImpl implements SupportService {
	
	@Override
	public void queryNotReturnRecord(String sql) {
		try {
			Connection conn = DBConnection.conn;
			Statement st = conn.createStatement();
			st.execute(sql);
//			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
