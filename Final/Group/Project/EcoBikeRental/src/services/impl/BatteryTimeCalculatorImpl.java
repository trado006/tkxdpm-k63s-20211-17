package services.impl;

import entity.Bike;
import entity.BikeType;
import services.BatteryTimeCalculator;
import services.BikeTypeService;
import services.FeeService;

public class BatteryTimeCalculatorImpl implements BatteryTimeCalculator {

	@Override
	public String estimateTimeRemain(int battery) {
		int hour = battery / 30;
		int minute = battery % 30;
		return hour + " giờ " + minute + "phút";
	}

}
