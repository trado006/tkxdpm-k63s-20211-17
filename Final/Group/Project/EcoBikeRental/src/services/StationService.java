package services;

import java.util.List;
import java.util.Map;

import entity.Station;

public interface StationService {
	
	public List<Station> getAllStation();
	
	public List<Station> getStationByName(String name);
	
	public List<Station> getStationByAddress(String address);
	
	public Station getStationById(int id);
	
	public int getNumberOfAvailBike(String stationName);
	
	public int getNumberOfAllBike(int stationId);
	
	public boolean checkNameStationAvailable(String name);
	
	public boolean addStation(Station station);

}
