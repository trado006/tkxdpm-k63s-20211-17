package services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.Bike;

public interface BatteryTimeCalculator {
	public String estimateTimeRemain (int battery);
}
