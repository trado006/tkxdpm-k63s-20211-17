package services;

import entity.Transaction;

public interface TransactionService {
	public Transaction getLastTranscationbByBikeAndUser(int bikeId, int userId);
	public boolean addTransaction(Transaction transaction);

}
