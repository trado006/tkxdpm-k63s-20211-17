package services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.Bike;

public interface BikeService {
	public List<Bike> getAllBikes();
	public List<Bike> searchBikes(HashMap<String, String> searchInfo);
	public List<Bike> getAllRentedBikes();
	public List<Bike> getAllBikeOnAnyStation();
	public List<Bike> getBikesByPlate(String plate);
	public List<Bike> getBikesByName(String name);
	public List<Bike> getBikesByType(String type);
	public List<Bike> getBikesByStation(String station);
	public List<Bike> getAllRentedBikeByUser(int userId);
	public List<Integer> getAllFillSlotsInStations(int stationId);
	public List<Bike> GetAllBikesByStationId(int stationId);
	public boolean addBike(Bike bike);
	public boolean updateReturnBike(int bikeId, int stationId, int slot);
	public boolean updateRentBike(int id, int stationId, int slot);
	public int getNewId();
}
