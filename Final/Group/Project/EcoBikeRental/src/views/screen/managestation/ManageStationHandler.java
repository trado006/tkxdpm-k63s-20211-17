package views.screen.managestation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import controllers.AddStationController;
import controllers.ManageStationController;
import entity.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import utils.Configs;
import views.alert.CustomAlert;
import views.dialog.addstation.AddStationDialog;
import views.screen.BaseHandler;
import views.screen.managebike.BikeAdminHandler;

public class ManageStationHandler extends BaseHandler {
	
    @FXML
    private ImageView imgHome;
    
    @FXML
    private ImageView releaseBtn;
    
    @FXML
    private ComboBox<String> typeComboBox;
    
    ObservableList<String> list = FXCollections.observableArrayList("name", "address");
    
    @FXML
    private TextField searchInput;

    @FXML
    private TableView<Station> stationTable;
    
    private ObservableList<Station> stationList;
    
    private List<Station> stationListSource;

    @FXML
    private TableColumn<Station, Integer> idCol;
    
    @FXML
    private TableColumn<Station, String> nameCol;
    
    @FXML
    private TableColumn<Station, String> addressCol;
    
    @FXML
    private TableColumn<Station, Integer> maxslotCol;

    @FXML
    private Button addButton;
//    
//    @FXML
//    private Button updateButton;
//    
//    @FXML
//    private Button DeleteButton;

    @FXML
    private Button cancelButton;

    
    public ManageStationHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		// TODO Auto-generated constructor stub
		File file = new File("src/assets/icons/home.png");
		Image im = new Image(file.toURI().toString());
		imgHome.setImage(im);
		
		imgHome.setOnMouseClicked(e -> {
			try {
				this.getPrev().show();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		releaseBtn.setOnMouseClicked(e -> {
			getAllStation();
			displayStationList();
		});
		
		cancelButton.setOnMouseClicked(e -> {
			try {
				this.getPrev().show();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		typeComboBox.setItems(list);
		typeComboBox.getSelectionModel().select(0);
		
		typeComboBox.setOnAction((event) -> {
			//searchType = typeComboBox.getValue();
		});
		
		searchInput.setOnKeyPressed(event -> {
			if( event.getCode() == KeyCode.ENTER ) {
				String type = typeComboBox.getValue();
				String keyWord = searchInput.getText().toString();
				if(type=="name") {
					this.stationListSource = getBController().getStationByName(keyWord);
				}else {
					this.stationListSource = getBController().getStationByAddress(keyWord);
				}
				displayStationList();
			}
		});
		
		addButton.setOnAction(event -> {
			AddStationDialog addStationDialog = new AddStationDialog();
			Station newStation = addStationDialog.show();
			if(newStation!=null) {
				AddStationController addStationController = new AddStationController();
				if(addStationController.addStation(newStation)) {
					CustomAlert.notice("Add new station successfully");
					getAllStation();
					displayStationList();
				}else {
					CustomAlert.notice("Add new station fails");
				}
				
			}
		});
		
		stationList = FXCollections.observableArrayList(new Station(1,"cong an", "huw ai", 12));
        idCol.setCellValueFactory(new PropertyValueFactory<Station, Integer>("id"));
        nameCol.setCellValueFactory(new PropertyValueFactory<Station, String>("name"));
        addressCol.setCellValueFactory(new PropertyValueFactory<Station, String>("address"));
        maxslotCol.setCellValueFactory(new PropertyValueFactory<Station, Integer>("maxSlot"));
        stationTable.setItems(stationList);
	}
    
    public ManageStationController getBController() {
		return (ManageStationController) super.getBController();
		
	}
    
    public void initDataDisplay() {
		getAllStation();
		displayStationList();
	}
    
    private void getAllStation() {
		this.stationListSource = getBController().getAllStation();
	}
    
    private void displayStationList() {
		// clear vboxBike
    	stationList.setAll(this.stationListSource);
	}

}
