package views.screen.managestation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import controllers.AddBikeController;
import controllers.BaseController;
import controllers.ManageBikeController;
import controllers.SearchBikeController;
import entity.Bike;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import services.BikeService;
import services.impl.BikeServiceImpl;
import utils.Configs;
import views.screen.BaseHandler;

public class TestStationHandler extends BaseHandler implements Initializable {
	@FXML
	private VBox vboxBike;
	
	@FXML
	private ImageView imgHome;
	
	@FXML
	private ImageView imgRefresh;
	
	@FXML
	private ImageView imgSearch;
	
	@FXML
	private ImageView imgAdd;

	private List<Bike> listBike;
	
	public TestStationHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		
		// fix relative image path caused by fxml
//		File file = new File("src/assets/icons/home.png");
//		Image im = new Image(file.toURI().toString());
//		imgHome.setImage(im);
//		file = new File("src/assets/icons/refresh.png");
//		im = new Image(file.toURI().toString());
//		imgRefresh.setImage(im);
//		file = new File("src/assets/icons/search.png");
//		im = new Image(file.toURI().toString());
//		imgSearch.setImage(im);
//		file = new File("src/assets/icons/add.png");
//		im = new Image(file.toURI().toString());
//		imgAdd.setImage(im);
//		
//		imgAdd.setOnMouseClicked(e -> {
//			try {
//				BaseHandler addBike = new AddBikeHandler(this.stage, Configs.ADD_BIKE_PATH);
//				BaseController controller = new AddBikeController();
//				addBike.setBController(controller);
//				addBike.show();
//			}
//			catch(IOException e1) {
//				e1.printStackTrace();
//			}
//		});
//		
//		imgSearch.setOnMouseClicked(e -> {
//			try {
//				BaseHandler searchHandler = new SearchBikeHandler(Configs.SEARCH_BIKE_PATH, this);
//				BaseController controller = new SearchBikeController();
//				searchHandler.setBController(controller);
//				searchHandler.showAndWait();
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		});
//		
//		imgHome.setOnMouseClicked(e -> {
//			try {
//				this.getPrev().show();
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		});
//		
//		imgRefresh.setOnMouseClicked(e -> {
//			getAllBikes();
//			displayListBike();
//		});
	}
	
//	public void initDataDisplay() {
//		getAllBikes();
//		displayListBike();
//	}
//	
//	public ManageBikeController getBController() {
//		return (ManageBikeController) super.getBController();
//		
//	}
//	
//	public void setListBike(List<Bike> list) {
//		this.listBike = list;
//		displayListBike();
//	}
//	
//	private void getAllBikes() {
//		this.listBike = getBController().getAllBikes();
//	}
//	
//	private void displayListBike() {
//		// clear vboxBike
//		vboxBike.getChildren().clear();
//		
//		try {
//			int index = 1;
//			for (Bike bike: this.listBike) {
//				BikeAdminHandler bikeAdminHandler = new BikeAdminHandler(Configs.BIKE_ADMIN_PATH, this);
//				bikeAdminHandler.setBController(getBController());
//				bikeAdminHandler.setBike(bike, index++);
//				vboxBike.getChildren().add((Node) bikeAdminHandler.getContent());
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}	
//	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
}
