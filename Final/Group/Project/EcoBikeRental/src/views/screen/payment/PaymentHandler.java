package views.screen.payment;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import common.exception.NotEnoughBalanceException;
import controllers.BaseController;
import controllers.PaymentController;
import controllers.ResultController;
import entity.Bike;
import entity.BikeType;
import entity.Station;
import entity.payment.CreditCard;
import entity.payment.PaymentTransaction;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.BikeService;
import services.BikeTypeService;
import services.StationService;
import services.UpdateService;
import services.impl.BikeServiceImpl;
import services.impl.BikeTypeServiceImpl;
import services.impl.StationServiceImpl;
import services.impl.UpdateReturnBikeServiceImpl;
import subsystem.InterbankSubsystem;
import utils.Configs;
import utils.Utils;
import views.screen.BaseHandler;
import views.screen.userhomepage.UserHomepageHandler;

public class PaymentHandler extends BaseHandler implements Initializable {

	@FXML
	private Label feeLabel;

	@FXML
	private Label errorLabel;
	
	@FXML
	private JFXTextArea cardHolderNameField;

	@FXML
	private JFXTextArea cardNumberField;

	@FXML
	private JFXTextArea expirationDateField;

	@FXML
	private JFXTextArea securityCodeField;
	
	private int amount;


	BikeService bikeService = new BikeServiceImpl();
	StationService stationService = new StationServiceImpl();
	BikeTypeService bikeTypeService = new BikeTypeServiceImpl();
	InterbankSubsystem interbankSubsystem = new InterbankSubsystem();

	public PaymentHandler(Stage stage, String screenPath, int amount) throws IOException {
		super(stage, screenPath);
		this.amount = amount;
	}

	public void initDataDisplay() {
		feeLabel.setText(Utils.formatMoney(amount));
	}

	@FXML
	public void rentBikeBtn(Event event) {
		
		// when user click submit rent bike
		
		try {
			CreditCard creditCard = new CreditCard(cardNumberField.getText(), cardHolderNameField.getText(),
					securityCodeField.getText(), expirationDateField.getText());
			PaymentTransaction paymentTransaction;
			if(amount > 0) {
				paymentTransaction = interbankSubsystem.payOrder(creditCard, amount, "rent");
			}else {
				paymentTransaction = interbankSubsystem.refund(creditCard, amount, "rent");
			}
			
			if(paymentTransaction.getResponseCode().equals("00")) {
				PaymentController paymentController =(PaymentController) this.getBController();
				paymentController.setSessionInvoice(creditCard, paymentTransaction.toString());
				paymentController.updateTransaction();
			}
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			ResultHandler resultHandler = new ResultHandler(primaryStage, Configs.RESULT_SCREEN_PATH, paymentTransaction);
			BaseController baseController = new ResultController();
			resultHandler.setBController(baseController);
			resultHandler.show();
		}catch (NotEnoughBalanceException e) { 
			errorLabel.setText("Không đủ số dư!");
		}
		catch (Exception e) {
			e.printStackTrace();
			errorLabel.setText("Thông tin thẻ không chính xác !");
			
		}
	}

	@FXML
	public void goBack(Event event) {
		try {
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			UserHomepageHandler userHomepageHandler = new UserHomepageHandler(primaryStage, Configs.USER_HOMEPAGE_PATH);
			BaseController baseController = new BaseController();
			userHomepageHandler.setBController(baseController);
			userHomepageHandler.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}
}
