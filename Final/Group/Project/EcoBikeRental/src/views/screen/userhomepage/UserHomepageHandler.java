package views.screen.userhomepage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import controllers.BaseController;
import controllers.ListRentedController;
import entity.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Bloom;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.StationService;
import services.impl.StationServiceImpl;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.returnbikes.RentedBikesHandler;
import views.screen.stationdetail.StationDetailHandler;

public class UserHomepageHandler extends BaseHandler implements Initializable {

	@FXML
	private AnchorPane mapPane;

	@FXML
	private ImageView rentingBikesBtn;

	@FXML
	private TextField stationSearchField;

	public UserHomepageHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// navigate to list renting bike of user
		rentingBikesBtn.setOnMouseClicked(e -> {
			RentedBikesHandler rentedBikesHandler;
			try {
				rentedBikesHandler = new RentedBikesHandler(this.stage, "/views/ListRentBike.fxml");
				BaseController baseController = new ListRentedController();
				rentedBikesHandler.setBController(baseController);
				rentedBikesHandler.setPrev(this);
				rentedBikesHandler.show();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		});
		// get all stations
		StationService stationService = new StationServiceImpl();
		List<Station> listStation = stationService.getAllStation();
		showStationOnMap(listStation);
	}

	@FXML
	public void searchStation() {
		String name = stationSearchField.getText();
		StationService stationService = new StationServiceImpl();
		List<Station> listStation = stationService.getStationByName(name);
		hiddenStation();
		showStationOnMap(listStation);
	}

	public void hiddenStation() {
		List<Node> listNode = mapPane.getChildren();
		mapPane.getChildren().removeAll(listNode);
	}

	public void showStationOnMap(List<Station> listStation) {
		for (Station station : listStation) {
			// display icon station on map
//			int x = Support.getRandomFrom2Int(50, 750);
//			int y = Support.getRandomFrom2Int(50, 350);
			int x = station.getId() * 150;
			int y = station.getId() * 75;
			Circle circle = new Circle(x, y, 11);
			Bloom bloom = new Bloom();
			bloom.setThreshold(0.0);
			circle.setEffect(bloom);
			circle.getStyleClass().addAll("circle-station", "station-element");
			Text text = new Text(Integer.toString(station.getId()));
			text.setLayoutX(x - 5);
			text.setLayoutY(y + 5);
			text.getStyleClass().addAll("text-station", "station-element");
			Tooltip.install(text, new Tooltip(station.getName()));
			// handle event click on station icon
			text.setOnMouseClicked(new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					try {
						Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						StationDetailHandler stationDetailHandler = new StationDetailHandler(primaryStage,
								Configs.STATION_DETAIL_PATH, station);
						BaseController baseController = new BaseController();
						stationDetailHandler.setBController(baseController);
						stationDetailHandler.show();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});

			// add to map
			mapPane.getChildren().addAll(circle, text);
		}
	}
}
