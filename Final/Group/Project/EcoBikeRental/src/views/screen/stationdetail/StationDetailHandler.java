package views.screen.stationdetail;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import controllers.BaseController;
import controllers.BikeDetailController;
import entity.Bike;
import entity.Station;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.BikeService;
import services.StationService;
import services.impl.BikeServiceImpl;
import services.impl.StationServiceImpl;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.bikedetail.BikeDetailHandler;
import views.screen.userhomepage.UserHomepageHandler;

public class StationDetailHandler extends BaseHandler implements Initializable {

	@FXML
	private Text name;

	@FXML
	private Text address;

	@FXML
	private Text maxSlot;

	@FXML
	private Text availBike;

	@FXML
	private Text emptySlot;

	@FXML
	private Button backBtn;

	@FXML
	private GridPane bikeGrid;

	private Station station;

	private static boolean stationSlotMap[][] = new boolean[5][3];
	BikeService bikeService = new BikeServiceImpl();
	StationService stationService = new StationServiceImpl();

	public StationDetailHandler(Stage stage, String screenPath, Station station) throws IOException {
		super(stage, screenPath);
		this.station = station;
	}

	public void initDataDisplay() {
		// init stationSlotMap
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 3; j++) {
				stationSlotMap[i][j] = false;
			}
		}

		// hien thi thong tin station
		int numAvailBike = stationService.getNumberOfAvailBike(station.getName());
		int numFreeSlot = station.getMaxSlot() - stationService.getNumberOfAllBike(station.getId());

		name.setText(station.getName());
		address.setText(station.getAddress());
		maxSlot.setText(station.getMaxSlot() + "");
		availBike.setText(numAvailBike + "");
		emptySlot.setText(numFreeSlot + "");
		name.setLayoutX(46);

		// hien thi so do danh sach xe
		List<Bike> listBike = bikeService.GetAllBikesByStationId(station.getId());
		for (Bike bike : listBike) {
			showBikeSlot(bike);
		}

		// hien thi cac cho trong con lai
		for (int i = 1; i <= station.getMaxSlot(); i++) {
			int x = (i + 2) % 3;
			int y = (i - 1) / 3;
			if (!stationSlotMap[y][x]) {
				Button bikeSlot = new Button();
				bikeSlot.setText("Trống");
				bikeSlot.getStyleClass().add("bike-slot");
				bikeSlot.getStyleClass().add("bike-slot-free");
				bikeGrid.add(bikeSlot, x, y);
			}
		}

	}

	@FXML
	public void goBack(Event event) {
		try {
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			UserHomepageHandler userHomepageHandler = new UserHomepageHandler(primaryStage, Configs.USER_HOMEPAGE_PATH);
			BaseController baseController = new BaseController();
			userHomepageHandler.setBController(baseController);
			userHomepageHandler.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void showBikeSlot(Bike bike) {
		int slot = bike.getSlot();
		int x = (slot + 2) % 3;
		int y = (slot - 1) / 3;
		Button bikeSlot = new Button();
		bikeSlot.getStyleClass().add("bike-slot");
		if (bike.isRenting()) {
			bikeSlot.setText("Đang thuê");
			bikeSlot.getStyleClass().add("bike-slot-renting");
		} else {
			bikeSlot.setText(bike.getName());
			bikeSlot.getStyleClass().add("bike-slot-avail");
			bikeSlot.setOnMouseClicked(new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					try {
						Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						BikeDetailHandler bikeDetailHandler = new BikeDetailHandler(primaryStage,
								Configs.BIKE_DETAIL_PATH, bike);
						BaseController baseController = new BikeDetailController();
						bikeDetailHandler.setBController(baseController);
						bikeDetailHandler.show();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});
		}
		bikeGrid.add(bikeSlot, x, y);
		stationSlotMap[y][x] = true;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}
}
