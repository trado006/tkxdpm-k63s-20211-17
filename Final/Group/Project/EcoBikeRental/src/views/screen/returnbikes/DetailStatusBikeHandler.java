package views.screen.returnbikes;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import controllers.BaseController;
import controllers.DetailStatusBikeController;
import controllers.ListRentedController;
import controllers.PaymentController;
import entity.Bike;
import entity.Session;
import entity.Station;
import entity.Transaction;
import entity.payment.InvoiceReturnBike;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.impl.UpdateReturnBikeServiceImpl;
import utils.Configs;
import views.screen.BaseHandler;
import views.screen.payment.PaymentHandler;

public class DetailStatusBikeHandler extends BaseHandler implements Initializable {

	@FXML
	private Text nameLabel;
	@FXML
	private Text timeLabel;
	@FXML
	private Text depositLabel;
	@FXML
	private Text feeLabel;
	
	@FXML
	private Label erorLabel;
	

	@FXML
	private ComboBox<String> stationStatusDropdown;

	@FXML
	private ComboBox<Integer> slotDropdown;

	@FXML
	private Button submitBtn;

	@FXML
	private Button backBtn;

	private Map<Station, List<Integer>> statusStations;

	private Bike bike;

	public DetailStatusBikeHandler(Stage stage, String screenPath, Bike bike) throws IOException {
		super(stage, screenPath);
		this.bike = bike;
	}

	private List<Integer> getFreeSlotByStationId(int stationId) {
		for (Station station : statusStations.keySet()) {
			if (station.getId() == stationId) {
				return statusStations.get(station);
			}
		}
		return null;
	}

	public void initDataDisplay() {
		DetailStatusBikeController detailStatusBikeController = (DetailStatusBikeController) this.getBController();

		nameLabel.setText(bike.getName() + " (ID: " + bike.getId() + ")");
		
		String pattern = "dd/MM/yyyy HH:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);
		String timeStr = df.format(detailStatusBikeController.getStartTime(bike.getId()));
		timeLabel.setText(timeStr);
		int deposit = detailStatusBikeController.getDepositFee(bike.getId());
		depositLabel.setText(Integer.toString(deposit));
		int fee = detailStatusBikeController.getFeeTotal(bike.getId(),bike.getType());
		feeLabel.setText(Integer.toString(fee));
		this.statusStations = detailStatusBikeController.getStatusStations();
		List<String> lstStr = new ArrayList<>();
		for (Station station : statusStations.keySet()) {
			lstStr.add(station.getId() + "-" + station.getName() + " (" + statusStations.get(station).size()
					+ " chỗ trống) ");
		}
		stationStatusDropdown.getItems().addAll(lstStr);

		backBtn.setOnMouseClicked(e -> {
			try {
				this.getPrev().show();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});
		
		submitBtn.setOnMouseClicked(e ->{
			try {
				String valueSelected = stationStatusDropdown.getValue();
				int idStation = Integer.parseInt(valueSelected.substring(0, valueSelected.indexOf("-")));
				int idSlot = slotDropdown.getValue();
				if(idStation > 0 && idSlot > 0) {
					Transaction transaction = new Transaction();
					transaction.setBikeId(bike.getId());
					transaction.setFee(fee-deposit);
					transaction.setStationFrom(bike.getStationId());
					transaction.setStationTo(idStation);
					bike.setSlot(idSlot);
					bike.setStationId(idStation);
					bike.setRenting(false);
					InvoiceReturnBike invoiceReturnBike = new InvoiceReturnBike(bike, null, transaction);
					detailStatusBikeController.setSessionInvoice(invoiceReturnBike);
					try {
						PaymentHandler paymentHandler = new PaymentHandler(this.stage, Configs.PAYMENT_PATH, (fee-deposit)/1000);
						PaymentController baseController = new PaymentController();
						baseController.setUpdateService(new UpdateReturnBikeServiceImpl());
						paymentHandler.setBController(baseController);
						paymentHandler.show();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else {
					erorLabel.setText("Chọn bãi xe và chỗ trống!");
				}
				
			}catch(Exception ex) {
				erorLabel.setText("Chọn bãi xe và chỗ trống!");
				ex.printStackTrace();
			}
			
			
		});

		// Set on action
		stationStatusDropdown.setOnAction(e -> {
			String valueSelected = stationStatusDropdown.getValue();
			int idStation = Integer.parseInt(valueSelected.substring(0, valueSelected.indexOf("-")));
			List<Integer> lstFreeSlot = getFreeSlotByStationId(idStation);
			slotDropdown.getItems().clear();
			slotDropdown.getItems().addAll(lstFreeSlot);
		});
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
//		Map<Station, List<Integer>> statusStationss = new HashMap<>();
//		for (int i = 1; i < 6; i++) {
//			Station station = new Station();
//			station.setId(i);
//			station.setName("bãi xe điện " + i);
//			List<Integer> tmp = new ArrayList<>();
//			for(int j=i; j>2; j--) {
//				tmp.add(j);
//			}
//			statusStationss.put(station, tmp);
//		}

	}

}
