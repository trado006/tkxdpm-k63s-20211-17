package views.screen.returnbikes;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import entity.Bike;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import views.screen.BaseHandler;

public class BikeHandler extends BaseHandler implements Initializable{
	private Bike bike;
	public BikeHandler(String screenPath, Bike bike) throws IOException {
		super(screenPath);
		this.bike = bike;
	}
	@FXML
	private ImageView image;
	@FXML
	private Label coLabel;
	@FXML
	private Label nameBikeLabel;
	@FXML
	private Label idBikeLabel;
	
	public void initDataDisplay() {
		nameBikeLabel.setText(bike.getName());
		idBikeLabel.setText(Integer.toString(bike.getId()));
		coLabel.setText(bike.getManufacturer());
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}

}
