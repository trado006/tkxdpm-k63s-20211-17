package entity.payment;

public class PaymentTransaction {
	private String responseCode;
	private PaymentCard card;
	private String transactionId;
	private String transactionContent;
	private int amount;
	private String createdAt;

	public PaymentTransaction(String responseCode, PaymentCard card, String transactionId, String transactionContent,
			int amount, String createdAt) {
		super();
		this.responseCode = responseCode;
		this.card = card;
		this.transactionId = transactionId;
		this.transactionContent = transactionContent;
		this.amount = amount;
		this.createdAt = createdAt;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public PaymentCard getCard() {
		return card;
	}

	public void setCard(PaymentCard card) {
		this.card = card;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getTransactionContent() {
		return transactionContent;
	}

	public int getAmount() {
		return amount;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String toString() {
		return responseCode + "-" + transactionId + "-" + transactionContent + "-" + amount;

	}
}
