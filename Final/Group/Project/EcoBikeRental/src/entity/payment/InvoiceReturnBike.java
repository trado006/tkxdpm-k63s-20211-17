package entity.payment;

import entity.Bike;
import entity.Transaction;

public class InvoiceReturnBike {
	private Bike bike;
	private PaymentCard paymentCard;
	private Transaction transaction;
	public InvoiceReturnBike(Bike bike, PaymentCard paymentCard, Transaction transaction) {
		super();
		this.bike = bike;
		this.paymentCard = paymentCard;
		this.transaction = transaction;
	}
	public Bike getBike() {
		return bike;
	}
	public void setBike(Bike bike) {
		this.bike = bike;
	}
	public PaymentCard getPaymentCard() {
		return paymentCard;
	}
	public void setPaymentCard(PaymentCard paymentCard) {
		this.paymentCard = paymentCard;
	}
	public Transaction getTransaction() {
		return transaction;
	}
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	
}
