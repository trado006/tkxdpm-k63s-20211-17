package entity;

import entity.payment.InvoiceReturnBike;

public class Session {
	private static User user;
	private static InvoiceReturnBike invoiceReturnBike;
	public static InvoiceReturnBike getInvoiceReturnBike() {
		return invoiceReturnBike;
	}

	public static void setInvoiceReturnBike(InvoiceReturnBike invoiceReturnBike) {
		Session.invoiceReturnBike = invoiceReturnBike;
	}

	public static User getUserLogin() {
		return user;
	}
	
	public static User setUserLogin(User u) {
		user = u;
		return user;
	}
	
	public static void logout() {
		user = null;
	}
}
