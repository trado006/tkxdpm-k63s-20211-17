package entity;

import java.sql.Date;

public class Bike {
	private int id;
	private int type;
	private int stationId;
	private boolean isRenting;
	
	private int slot;
	private String name;
	private float weight;
	
	private String manufacturer;
	private String licensePlate;
	private int price;
	
	private Date manufacturingDate;
	private int loadCycles;
	private int batteryPercentage;
	
	public Bike() {
		
	}
	
	public Bike(int id, int type, int stationId, boolean isRenting, int slot, String name, float weight,
			String manufacturer, String licensePlate, int price, Date manufacturingDate, int loadCycles, int batteryPercentage) {
		this.id = id;
		this.type = type;
		this.stationId = stationId;
		this.isRenting = isRenting;
		this.slot = slot;
		this.name = name;
		this.weight = weight;
		this.manufacturer = manufacturer;
		this.licensePlate = licensePlate;
		this.price = price;
		this.manufacturingDate = manufacturingDate;
		this.loadCycles = loadCycles;
		this.batteryPercentage = batteryPercentage;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getStationId() {
		return stationId;
	}
	public void setStationId(int stationId) {
		this.stationId = stationId;
	}
	public boolean isRenting() {
		return isRenting;
	}
	public void setRenting(boolean isRenting) {
		this.isRenting = isRenting;
	}
	public boolean getRenting() {
		return this.isRenting;
	}
	public int getSlot() {
		return slot;
	}
	public void setSlot(int slot) {
		this.slot = slot;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Date getManufacturingDate() {
		return manufacturingDate;
	}
	public void setManufacturingDate(Date manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}
	public int getLoadCycles() {
		return loadCycles;
	}
	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}
	public int getBatteryPercentage() {
		return batteryPercentage;
	}
	public void setBatteryPercentage(int batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

}
