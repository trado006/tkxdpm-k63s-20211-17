package entity;

public class BikeType {
	private int id;
	private String content;
	private int deposit;
	
	public BikeType() {
		// TODO Auto-generated constructor stub
	};
	
	public BikeType(int id, String content, int deposit) {
		this.setId(id);
		this.setContent(content);
		this.setDeposit(deposit);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}
	
}
