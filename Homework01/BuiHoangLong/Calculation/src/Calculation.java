
import java.util.Scanner;

public class Calculation {

	public static void main(String[] args) {
		
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Enter 2 number: ");
			int a = scanner.nextInt();
			int b = scanner.nextInt();
			
			System.out.println("a + b = " + (a + b));
			System.out.println("a - b = " + (a - b));
			System.out.println("a * b = " + (a * b));
			System.out.println("a / b = " + (a / b));
		}

	}

}